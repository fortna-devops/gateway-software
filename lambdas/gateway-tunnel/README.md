# SSH tunnel management tool
This lambda can't be run locally.

## Usage

### Preparations

* Go to [aws mqtt client][aws-mqtt-client]
* Set "Subscription topic"to `GATEWAY_NAME/ssh-tunnel` (e.g. mhs-testloop-atlanta/ssh-tunnel)
* Do not touch "Quality of Service"
* Set "MQTT payload display" to `Display payloads as strings (more accurate)`
* Click "Subscribe to topic"
* Set "Publish" to `GATEWAY_NAME/ssh-tunnel/invoke` (e.g mhs-testloop-atlanta/ssh-tunnel/invoke)

Now you should be ready to communicate with the gateway

### Get tunnel status

Send: 
```json
{
  "action": "status"
}
```

You should receive something like:
```json
{
  "status": "success", 
  "tunnel_status": "stopped"
}
```

### Start tunnel

Send: 
```json
{
  "action": "start"
}
```

You should receive something like:
```json
{
  "status": "success", 
  "params": {
    "local_host": "localhost", 
    "local_port": 22, 
    "server_host": "ec2-3-87-4-84.compute-1.amazonaws.com", 
    "server_port": 22, 
    "server_user": "ec2-user", 
    "server_identity_file": "./ssh-reverse-forward.pem", 
    "forward_port": 9000, 
    "ttl": 3600
  }
}
```

You can override `params` for ssh tunnel by specifying `payload` key. e.g.
```json
{
  "action": "start",
  "payload": {
    "forward_port": 9001
  }
}
```

#### Available tunnel params

* `local_host` - host to which tunnel should point (default "localhost") *DO NOT TOUCH*
* `local_port` - port to point (default 22)
* `server_host` - ssh server address to connect (default "ec2-3-87-4-84.compute-1.amazonaws.com")
* `server_port` - ssh server port (default 22)
* `server_user` - ssh server user name
* `server_identity_file` - key file to use for authentication (default "./ssh-reverse-forward.pem") It's a part of this lambda.
* `forward_port` - port that should be used on remote server (default 9000)
* `ttl` - ssh tunnel TTL in seconds (default 3600). Tunnel will be stopped automatically after this time.


### Stop tunnel

Send:
```json
{
  "action": "stop"
}
```

You should receive something like:
```json
{
  "status": "success"
}
```


### Connecting to the gateway

* start tunnel
* ssh to remote server (e.g. `ssh -i ./ssh-reverse-forward.pem ec2-user@ec2-3-87-4-84.compute-1.amazonaws.com`)
* remove known hosts (e.g `rm -rf /home/ec2-user/.ssh/known_hosts`). Not the best solution, but it works.
* ssh to the gateway using tunnel (e.g. `ssh -p 9000 mhs-predict@localhost`)

[aws-mqtt-client]: https://us-east-1.console.aws.amazon.com/iot/home?region=us-east-1#/test
