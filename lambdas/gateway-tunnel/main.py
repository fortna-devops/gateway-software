"""
Communicates over MQTT with AWS cloud and can setup reverse proxy ssh tunnel to external server
"""

import os
import json
import logging
import select
import socket
import threading

import greengrasssdk
import paramiko

logger = logging.getLogger()


class SSHTunnel():
    """
    Communicates over MQTT with AWS cloud and can setup reverse proxy ssh tunnel to external server
    """

    start_action = 'start'
    stop_action = 'stop'
    status_action = 'status'

    def __init__(self, gateway_name: str):
        self._transport_th = None
        self._timer = None
        self._stopped = threading.Event()

        self._ssh_client = paramiko.SSHClient()
        self._ssh_client.load_system_host_keys()
        self._ssh_client.set_missing_host_key_policy(paramiko.WarningPolicy())

        self._action_map = {
            self.start_action: self.start,
            self.stop_action: self.stop,
            self.status_action: self.status,
        }

        self.mqtt_topic = f'{gateway_name}/ssh-tunnel'

        self._client = greengrasssdk.client('iot-data')

    def invoke(self, event: dict):
        """
        Handler for MQTT messages from AWS
        :param event: AWS event
        :return:
        """
        result = {}
        try:
            action = event.get('action', None)
            self._validate(action)

            func = self._action_map[action]
            payload = event.get('payload', {})
            result = func(**payload)
        except BaseException as e:  # pylint: disable=W0703
            logger.error('An error occurred while processing event', exc_info=e)
            result = {'status': 'error', 'msg': str(e)}
        finally:
            self._publish(result)

    def _validate(self, action: str):
        """
        Validate action
        :param action: action name
        :return:
        """
        if action is None:
            raise ValueError('No action specified')
        if action not in self._action_map:
            raise ValueError(f'Unsupported action \'{action}\'')

    def _publish(self, message: dict):
        """
        Send message to MQTT topic
        :param message: message to send
        :return:
        """
        self._client.publish(topic=self.mqtt_topic, payload=json.dumps(message))

    def _is_alive(self) -> bool:
        """
        Check tunnel state
        :return: True in case tunnel is up and running
        """
        return self._transport_th is not None and self._transport_th.is_alive()

    def _handler(self, chan, host, port):
        """
        Handle ssh tunnel communication
        :param chan:
        :param host:
        :param port:
        :return:
        """
        sock = socket.socket()
        try:
            sock.connect((host, port))
        except Exception as e:  # pylint: disable=W0703
            logger.info(f'Forwarding request to {host}:{port} failed: {e}')
            return

        logger.info(f'Connected! Tunnel open {chan.origin_addr} -> {chan.getpeername()} -> ({host}, {port})')
        while not self._stopped.is_set():
            resp, *_ = select.select([sock, chan], [], [])
            if sock in resp:
                data = sock.recv(1024)
                if not data:
                    break
                chan.send(data)
            if chan in resp:
                data = chan.recv(1024)
                if not data:
                    break
                sock.send(data)
        chan.close()
        sock.close()
        logger.info(f'Tunnel closed from {chan.origin_addr}')

    def _reverse_forward_tunnel(self, server_port: int, remote_host: str, remote_port: int, transport: paramiko.Transport):
        """
        Setup tunnel in the separate thread
        :param server_port: ssh server port
        :param remote_host: host to setup tunnel to
        :param remote_port: port to setup tunnel to
        :param transport: paramiko.Transport to use
        :return:
        """
        transport.request_port_forward('', server_port)
        while not self._stopped.is_set():
            chan = transport.accept(1)
            if chan is None:
                continue
            thr = threading.Thread(target=self._handler, daemon=True, args=(chan, remote_host, remote_port))
            thr.start()

    def start(self, **kwargs) -> dict:
        """
        Start ssh tunnel
        "start" action handler
        :param kwargs: tunnel parameters
        :return: status of action
        """
        kwargs.setdefault('local_host', 'localhost')
        kwargs.setdefault('local_port', 22)
        kwargs.setdefault('server_host', 'ec2-3-87-4-84.compute-1.amazonaws.com')
        kwargs.setdefault('server_port', 22)
        kwargs.setdefault('server_user', 'ec2-user')
        kwargs.setdefault('server_identity_file', './ssh-reverse-forward.pem')
        kwargs.setdefault('forward_port', 9000)
        kwargs.setdefault('ttl', 3600)

        if self._is_alive():
            return {'status': 'error', 'msg': 'Tunnel already started'}

        self._stopped.clear()
        self._ssh_client.connect(
            kwargs['server_host'],
            kwargs['server_port'],
            username=kwargs['server_user'],
            key_filename=kwargs['server_identity_file']
        )

        args = (kwargs['forward_port'], kwargs['local_host'], kwargs['local_port'],
                self._ssh_client.get_transport())
        self._transport_th = threading.Thread(target=self._reverse_forward_tunnel, args=args)
        self._transport_th.setDaemon(True)
        self._transport_th.start()

        if kwargs['ttl']:
            self._timer = threading.Timer(kwargs['ttl'], self.stop)
            self._timer.start()

        return {'status': 'success', 'params': kwargs}

    def stop(self) -> dict:
        """
        Stop tunnel
        "stop" action handler
        :return: status of action
        """
        if not self._is_alive():
            return {'status': 'error', 'msg': 'Tunnel is not started'}

        if self._timer is not None and not self._timer.finished.is_set():
            self._timer.cancel()

        self._stopped.set()
        self._ssh_client.close()
        self._transport_th.join()
        return {'status': 'success'}

    def status(self) -> dict:
        """
        "status" action handler
        :return: status of tunnel
        """
        return {
            'status': 'success',
            'tunnel_status': 'running' if self._is_alive() else 'stopped'
        }


GATEWAY_NAME = os.getenv('GATEWAY')
TUNNEL = SSHTunnel(GATEWAY_NAME)


def handler(event, _context):
    """
    Handler for MQTT messages
    :param event: MQTT message
    :param _context: context
    :return:
    """
    TUNNEL.invoke(event)
