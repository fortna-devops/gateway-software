"""
Mixin class for banner devices
"""

from .qm42vt2_mixin import QM42VT2Mixin
from .m12fth3q_mixin import M12FTH3QMixin
