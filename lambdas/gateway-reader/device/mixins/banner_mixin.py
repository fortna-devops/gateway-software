"""
Mixin for banner devices
"""

from abc import ABC, abstractmethod


__all__ = ['BannerMixin']


class BannerMixin(ABC):
    """
    Banner sensor data mixin.
    """

    @property
    @abstractmethod
    def _data_points(self):
        return ()

    @property
    @abstractmethod
    def _holding_registers(self):
        return ()

    def _normalize(self, raw_data):
        data = []
        for value, (c_type, scale) in zip(raw_data, self._data_points):
            data.append(c_type(value).value / scale)

        return data
