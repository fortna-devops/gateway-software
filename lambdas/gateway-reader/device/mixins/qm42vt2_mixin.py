"""
QM42VT2 Mixin class
"""

import ctypes

from .banner_mixin import BannerMixin


__all__ = ['QM42VT2Mixin']


class QM42VT2Mixin(BannerMixin):
    """
    Banner qm42vt2 sensor data mixin.
    """

    # Specifies types and scale of data.
    _data_points = (
        (ctypes.c_uint16, 10000.),  # rms_velocity_z
        (ctypes.c_uint16, 10000.),  # rms_velocity_z (metric)
        (ctypes.c_int16, 100.),  # temperature
        (ctypes.c_int16, 100.),  # temperature (celsius)
        (ctypes.c_uint16, 10000.),  # rms_velocity_x
        (ctypes.c_uint16, 10000.),  # rms_velocity_x (metric)
        (ctypes.c_uint16, 1000.),  # peak_acceleration_z
        (ctypes.c_uint16, 1000.),  # peak_acceleration_x
        (ctypes.c_uint16, 10.),  # peak_frequency_z
        (ctypes.c_uint16, 10.),  # peak_frequency_x
        (ctypes.c_uint16, 1000.),  # rms_acceleration_z
        (ctypes.c_uint16, 1000.),  # rms_acceleration_x
        (ctypes.c_uint16, 1000.),  # kurtosis_z
        (ctypes.c_uint16, 1000.),  # kurtosis_x
        (ctypes.c_uint16, 1000.),  # crest_acceleration_z
        (ctypes.c_uint16, 1000.),  # crest_acceleration_x
        (ctypes.c_uint16, 10000.),  # peak_velocity_z
        (ctypes.c_uint16, 10000.),  # peak_velocity_z (metric)
        (ctypes.c_uint16, 10000.),  # peak_velocity_x
        (ctypes.c_uint16, 10000.),  # peak_velocity_x (metric)
        (ctypes.c_uint16, 1000.),  # hf_rms_acceleration_z
        (ctypes.c_uint16, 1000.),  # hf_rms_acceleration_x
    )

    _holding_registers = (5200, 22)
