"""
M12FTH3Q Mixin class
"""

import ctypes

from .banner_mixin import BannerMixin


__all__ = ['M12FTH3QMixin']


class M12FTH3QMixin(BannerMixin):
    """
    Banner m12fth3q sensor data mixin.
    """

    # Specifies types and scale of data.
    _data_points = (
        (ctypes.c_uint16, 100.),  # humidity (%RH)
        (ctypes.c_int16, 20.),  # temperature (C)
        (ctypes.c_int16, 20.),  # temperature (F)
    )

    _holding_registers = (0000, 3)
