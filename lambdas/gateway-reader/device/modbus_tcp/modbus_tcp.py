"""
ModBus over TCP base class
"""

from pyModbusTCP.client import ModbusClient

from ..base import BaseDevice


__all__ = ['ModbusTCPDevice']


class ModbusTCPDevice(BaseDevice):  # pylint: disable=W0223
    """
    ModBus over TCP base class
    """

    def __init__(self, port, address, read_interval, server_host, server_port,
                 timeout=None):
        super(ModbusTCPDevice, self).__init__(port, address, read_interval)

        address = int(address)
        self._server_host = server_host
        self._server_port = server_port

        self._client = ModbusClient(server_host, server_port, address, timeout,
                                    auto_open=True, auto_close=True)
