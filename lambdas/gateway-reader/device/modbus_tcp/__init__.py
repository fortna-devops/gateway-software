"""
ModBus over TCP base class
"""

from .modbus_tcp import ModbusTCPDevice
