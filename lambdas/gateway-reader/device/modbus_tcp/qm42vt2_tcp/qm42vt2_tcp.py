"""
QM42VT2 over TCP class
"""

from device.error import DeviceError
from device.mixins import QM42VT2Mixin
from .. import ModbusTCPDevice


__all__ = ['QM42VT2TCPDevice']


class QM42VT2TCPDevice(QM42VT2Mixin, ModbusTCPDevice):
    """
    Banner qm42vt2 over ModBusTCP sensor data reader.
    """

    type_key = 'qm42vt2_tcp'

    def _read(self):
        """
        Read raw data from sensor and apply modifications
        """
        raw_data = self._client.read_holding_registers(*self._holding_registers)
        if raw_data is None:
            raise DeviceError('Device on {}:{} returned "None"'
                              .format(self._server_host, self._server_port))
        data = self._normalize(raw_data)
        return data
