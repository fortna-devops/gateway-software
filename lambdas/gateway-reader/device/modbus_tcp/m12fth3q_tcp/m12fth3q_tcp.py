"""
M12FTH3Q over TCP class
"""

from device.error import DeviceError
from device.mixins import M12FTH3QMixin
from .. import ModbusTCPDevice


__all__ = ['M12FTH3QTCPDevice']


class M12FTH3QTCPDevice(M12FTH3QMixin, ModbusTCPDevice):
    """
    Banner m12fth3q over ModBusTCP sensor data reader.
    """

    type_key = 'm12fth3q_tcp'

    def _read(self):
        """
        Read raw data from sensor and apply modifications
        """
        raw_data = self._client.read_holding_registers(*self._holding_registers)
        if raw_data is None:
            raise DeviceError('Device on {}:{} returned "None"'
                              .format(self._server_host, self._server_port))
        data = self._normalize(raw_data)
        return data
