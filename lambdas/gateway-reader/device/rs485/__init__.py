"""
Base RS485 device class
"""

from .rs485 import RS485Device
