"""
Base RS485 device class
"""

from ..base import BaseDevice


__all__ = ['RS485Device']


class RS485Device(BaseDevice):  # pylint: disable=W0223
    """
    Base class for RS485 devices
    """
