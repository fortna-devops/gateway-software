"""
ModBusRTU base device class
"""

from typing import List, Any

import minimalmodbus

from .. import RS485Device


__all__ = ['ModbusDevice']


# Default timeout
# In minimalmodbus default timeout set to 0.05 but it's to low for most cases
TIMEOUT = 0.1

# Default baudrate
BAUDRATE = minimalmodbus.BAUDRATE


class ModbusDevice(RS485Device):  # pylint: disable=W0223
    """
    ModBusRTU base device class
    """

    def __init__(self, port, address, read_interval, timeout=TIMEOUT, baudrate=BAUDRATE):
        super(ModbusDevice, self).__init__(port, address, read_interval)
        address = int(self._address)
        self._instrument = minimalmodbus.Instrument(self._port, address)

        self.__timeout = timeout
        self.__baudrate = baudrate

    def read(self) -> List[Any]:
        """
        Read data from device
        :return: Raw data from device
        """
        self._instrument.serial.timeout = self.__timeout
        self._instrument.serial.baudrate = self.__baudrate
        return super(ModbusDevice, self).read()
