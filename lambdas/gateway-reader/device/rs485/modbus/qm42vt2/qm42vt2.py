"""
QM42VT2 over ModBusRTU device class
"""

from device.mixins import QM42VT2Mixin
from .. import ModbusDevice


__all__ = ['QM42VT2Device']


class QM42VT2Device(QM42VT2Mixin, ModbusDevice):
    """
    Banner qm42vt2 over ModBusRTU sensor data reader.
    """

    type_key = 'qm42vt2'

    def _read(self):
        """
        Read raw data from sensor and apply modifications
        """
        raw_data = self._instrument.read_registers(*self._holding_registers)
        data = self._normalize(raw_data)
        return data
