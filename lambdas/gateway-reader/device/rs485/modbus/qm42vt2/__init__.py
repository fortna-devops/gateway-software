"""
QM42VT2 over ModBusRTU device class
"""

from .qm42vt2 import QM42VT2Device
