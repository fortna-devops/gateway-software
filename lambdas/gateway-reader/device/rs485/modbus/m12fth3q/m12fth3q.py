"""
M12FTH3Q over ModBusRTU device class
"""

from device.mixins import M12FTH3QMixin
from .. import ModbusDevice


__all__ = ['M12FTH3QDevice']


class M12FTH3QDevice(M12FTH3QMixin, ModbusDevice):
    """
    Banner m12fth3q over ModBusRTU sensor data reader.
    """

    type_key = 'm12fth3q'

    def _read(self):
        """
        Read raw data from sensor and apply modifications
        """
        raw_data = self._instrument.read_registers(*self._holding_registers)
        data = self._normalize(raw_data)
        return data
