"""
Base Ethernet devices class
"""

from ..base import BaseDevice


__all__ = ['EthernetDevice']


class EthernetDevice(BaseDevice):  # pylint: disable=W0223
    """
    Base class for Ethernet devices
    """
