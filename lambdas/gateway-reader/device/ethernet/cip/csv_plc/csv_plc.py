"""
CSV PLC class
"""

import logging

from cpppo.server.enip import client

from device.error import DeviceError
from .. import CIPDevice


__all__ = ['CSVPLCDevice', 'PLCDeviceError']


logger = logging.getLogger()


class PLCDeviceError(DeviceError):
    """
    PLC device error class
    """


class CSVPLCDevice(CIPDevice):
    """
    Reads csv data from PLC
    """
    type_key = 'csv_plc'

    _depth = 1  # Allow 1 transaction in-flight
    _multiple = 0  # Don't use Multiple Service Packet
    _fragment = False  # Don't force Read/Write Tag Fragmented
    _timeout = 1.0  # Any PLC I/O fails if it takes > 1s
    _printing = False  # Print a summary of I/O
    _delimiter = ','  # Delimiter for PLC data string

    def __init__(self, port, address, read_interval, plc_host, plc_port, route_path):
        super(CSVPLCDevice, self).__init__(port, address, read_interval)
        self._plc_host = plc_host
        self._plc_port = plc_port
        self._route_path = route_path

    @property
    def address(self):
        return 'PLC-'+self._address.replace('_UPLOAD', '').replace('_', '-')

    def _read(self):
        """
        Initialize connection and parse data from PLC.
        :return: Parsed data from PLC.
        """
        with self._get_connection() as connection:
            raw_data = self._read_plc_string(connection, self._address)

        return raw_data.split(self._delimiter)

    def _read_plc_string(self, connection, tag):
        """
        Read raw data string from PLC.
        :param connection: Connector object.
        :param tag: Tag to read.
        :return: Raw data string.
        """
        logger.debug(f'Try to read PLC tag "{tag}"')
        operations = client.parse_operations([f'{tag}.LEN'], route_path=self._route_path)
        failures, transactions = connection.process(
            operations=operations, depth=self._depth, multiple=self._multiple,
            fragment=self._fragment, printing=self._printing, timeout=self._timeout
        )
        if failures:
            raise PLCDeviceError(f'Can\'t read data by tag "{tag}"')

        # always one transaction and len is a first byte
        data_len = transactions[0][0]

        operations = client.parse_operations([f'{tag}.DATA*{data_len}'], route_path=self._route_path)
        failures, transactions = connection.process(
            operations=operations, depth=self._depth, multiple=self._multiple,
            fragment=self._fragment, printing=self._printing,
            timeout=self._timeout
        )
        if failures:
            raise PLCDeviceError(f'Can\'t read data by tag "{tag}"')

        data = ''.join(map(chr, transactions[0]))
        logger.debug(f'Successfully read data from tag "{tag}": {data}')
        return data

    def _get_connection(self):
        """
        Get connection to PLC.
        :return: Connector object.
        """
        logger.debug(f'Establish connection to host: {self._plc_host}, '
                     f'port: {self._plc_port}, timeout: {self.last_read_time}')
        return client.connector(self._plc_host, self._plc_port, self._timeout)
