"""
Base CIP protocol device class
"""

from .. import EthernetDevice


__all__ = ['CIPDevice']


class CIPDevice(EthernetDevice):  # pylint: disable=W0223
    """
    Base class for CIP protocol devices
    """
