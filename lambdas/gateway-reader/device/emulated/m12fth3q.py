"""
M12FTH3Q Emulated class
"""

import random

from .base import BaseEmulatedDevice

__all__ = ['M12FTH3QEmulatedDevice']


class M12FTH3QEmulatedDevice(BaseEmulatedDevice):
    """
    Banner m12fth3q emulated data reader.
    """

    type_key = 'm12fth3q_emulated'

    def _get_emulation_schema(self):
        return (
            self.get_humidity,
            self.get_temperature_c,
            self.get_temperature,
        )

    @staticmethod
    def get_humidity():
        """
        Get humidity.
        :return: humidity in %RH
        """
        return random.randint(0, 100)

    @staticmethod
    def get_temperature_c():
        """
        Get temperature.
        :return: Temperature in Celsius.
        """
        return 20 + 10.0 * random.random()

    @staticmethod
    def get_temperature():
        """
        Get temperature.
        :return: Temperature in Fahrenheit.
        """
        return 70 + 5.0 * random.random()
