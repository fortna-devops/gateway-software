"""
Base emulated device class
"""

import random
import string

from abc import abstractmethod

from ..base import BaseDevice
from ..error import DeviceError


__all__ = ['BaseEmulatedDevice', 'EmulatedDeviceError']


def random_str():
    """
    Generate string of random length contains letters and digits
    :return: String of random length contains letters and digits
    """
    return ''.join(random.choice(string.ascii_letters + string.digits)
                   for _ in range(random.randrange(5, 10)))


def random_int():
    """
    Generate random integer number from 0 to 100
    :return: Random integer number from 0 to 100
    """
    return random.randint(0, 100)


def random_float():
    """
    Generate random float number from 0 to 100
    :return: Random float number from 0 to 100
    """
    return random.uniform(0, 100)


class EmulatedDeviceError(DeviceError):
    """
    Emulated device error
    """


class BaseEmulatedDevice(BaseDevice):
    """
    Provides logic for data emulation random read errors.
    """

    _emulation_map = {
        str: random_str,
        int: random_int,
        float: random_float
    }

    def __init__(self, *args, **kwargs):
        """
        Initialization.
        """
        self._error_rate = kwargs.pop('error_rate')
        super(BaseEmulatedDevice, self).__init__(*args, **kwargs)

    @abstractmethod
    def _get_emulation_schema(self):
        return ()

    def _get_random_value(self, type):  # pylint: disable=W0622
        """
        Returns random value based on type.
        :param type: type to emulate, should be one of from _emulated_map.
        :return: random value.
        """
        if type not in self._emulation_map:
            raise EmulatedDeviceError('Unsupported emulation type')

        return self._emulation_map[type]()

    def _read(self):
        """
        Fails on read based on "error_rate" or returns emulated data.
        """
        if random.random() <= self._error_rate:
            raise EmulatedDeviceError("Emulated error")

        data = []
        for field in self._get_emulation_schema():
            if isinstance(field, type):
                data.append(self._get_random_value(field))
            elif callable(field):
                data.append(field())
            else:
                raise EmulatedDeviceError('Emulation filed can be ether "type" '
                                          'or "function"')

        return data
