"""
Emulated device class
"""

from .base import BaseEmulatedDevice, EmulatedDeviceError
