"""
CSV PLC Emulated class
"""

import datetime
import random

from .base import BaseEmulatedDevice


__all__ = ['CSVPLCEmulatedDevice']


class CSVPLCEmulatedDevice(BaseEmulatedDevice):
    """
    CSV PLC Emulated class
    """

    type_key = 'csv_plc_emulated'

    def _get_emulation_schema(self):
        return (
            self.get_conveyor_name,
            self.get_date_time,
            self.get_plc_status,
            self.get_belt_speed,
            self.get_vfd_current,
            self.get_vfd_frequency,
            self.get_vfd_voltage,
            self.get_vfd_fault_code,
            self.get_sorter_induct_rate,
            self.get_sorter_reject_rate,
            self.get_status,
        )

    def get_conveyor_name(self):
        """
        Get conveyor name.
        :return: Conveyor name.
        """
        return self._get_random_value(str)

    @staticmethod
    def get_date_time():
        """
        Get datetime.
        :return: PLC datetime in format YYYY-MM-DD HH:MM:SS
        """
        now = datetime.datetime.utcnow()
        return now.strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def get_plc_status():
        """
        Get PLC status.
        :return: One of "RUN", "REM", "PPROGRAM"
        """
        return 'RUN'

    @staticmethod
    def get_belt_speed():
        """
        Get conveyor belt speed.
        :return: Speed in feet per minute.
        """
        return 95 + int(10 * random.random())

    @staticmethod
    def get_vfd_current():
        """
        Get VFD motor current.
        :return: Current in Amperes.
        """
        return 1.0 + random.random()

    @staticmethod
    def get_vfd_frequency():
        """
        Get VFD motor frequency.
        :return: Frequency in Hertz.
        """
        return 1000 + 50 * random.random()

    @staticmethod
    def get_vfd_voltage():
        """
        Get VFD motor voltage.
        :return: Voltage in Volts.
        """
        return 300.0 + 10.0 * random.random()

    @staticmethod
    def get_vfd_fault_code():
        """
        Get VFD motor fault code.
        :return: 0 when no error, non-zero otherwise
        """
        return 0

    @staticmethod
    def get_sorter_induct_rate():
        """
        1 minute average of packages entering sorter
        :return: Rate in packages per hour
        """
        return 0

    @staticmethod
    def get_sorter_reject_rate():
        """
        1 minute average of packages exiting(not sorted) sorter
        :return: Rate in packages per hour
        """
        return 0

    @staticmethod
    def get_status():
        """
        Get conveyor status.
        :return: One of "OFF", "RUNNING", "JAMMED", "ESTOPPED", "FAULTED".
        """
        return 'RUNNING'
