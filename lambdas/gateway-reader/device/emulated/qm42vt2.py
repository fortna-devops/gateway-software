"""
QM42VT2 Emulated class
"""

import random
from datetime import datetime

from .base import BaseEmulatedDevice

__all__ = [
    'QM42VT2EmulatedDevice',
    'QM42VT2EmulatedDeviceMTDAlarms',
    'QM42VT2EmulatedDeviceMLAlarms',
    'QM42VT2EmulatedDeviceIsoThreshRedAlarms',
    'QM42VT2EmulatedDeviceIsoThreshYellowAlarms',
]


class QM42VT2EmulatedDevice(BaseEmulatedDevice):
    """
    Banner qm42vt2 emulated data reader.
    """

    type_key = 'qm42vt2_emulated'

    def _get_emulation_schema(self):
        return (
            self.get_rms_velocity_z,
            self.get_rms_velocity_z,
            self.get_temperature,
            self.get_temperature_c,
            self.get_rms_velocity_x,
            self.get_rms_velocity_x,
            self.get_peak_acceleration_z,
            self.get_peak_acceleration_x,
            self.get_peak_frequency_z,
            self.get_peak_frequency_x,
            self.get_rms_acceleration_z,
            self.get_rms_acceleration_x,
            self.get_kurtosis_z,
            self.get_kurtosis_x,
            self.get_crest_acceleration_z,
            self.get_crest_acceleration_x,
            self.get_peak_velocity_z,
            self.get_peak_velocity_z,
            self.get_peak_velocity_x,
            self.get_peak_velocity_x,
            self.get_hf_rms_acceleration_z,
            self.get_hf_rms_acceleration_x,
        )

    @staticmethod
    def get_rms_velocity_z():
        """
        Get root mean square vibration velocity, Z axis.
        :return: Vibration velocity in inches per second.
        """
        return 0.05 + 0.04 * random.random()

    @staticmethod
    def get_temperature():
        """
        Get temperature.
        :return: Temperature in Fahrenheit.
        """
        return 70 + 5.0 * random.random()

    @staticmethod
    def get_temperature_c():
        """
        Get temperature.
        :return: Temperature in Celsius.
        """
        return 20 + 10.0 * random.random()

    @staticmethod
    def get_rms_velocity_x():
        """
        Get root mean square vibration velocity, X axis.
        :return: Vibration velocity in inches per second.
        """
        return 0.06 + 0.04 * random.random()

    @staticmethod
    def get_peak_acceleration_z():
        """
        Get peak acceleration, bandwidth 1..4 kHz, Z axis.
        :return: Acceleration in g.
        """
        return 0.4 + 0.2 * random.random()

    @staticmethod
    def get_peak_acceleration_x():
        """
        Get peak acceleration, bandwidth 1..4 kHz, X axis.
        :return: Acceleration in g.
        """
        return 0.5 + 0.2 * random.random()

    @staticmethod
    def get_peak_frequency_z():
        """
        Get peak velocity component frequency, Z axis.
        :return: Frequency in Hertz.
        """
        return 50 + 10.0 * random.random()

    @staticmethod
    def get_peak_frequency_x():
        """
        Get peak velocity component frequency, X axis.
        :return: Frequency in Hertz.
        """
        return 55 + 10.0 * random.random()

    @staticmethod
    def get_rms_acceleration_z():
        """
        Get root mean square acceleration, Z axis.
        :return: Acceleration in g.
        """
        return 0.2 + 0.1 * random.random()

    @staticmethod
    def get_rms_acceleration_x():
        """
        Get root mean square acceleration, X axis.
        :return: Acceleration in g.
        """
        return 0.3 + 0.1 * random.random()

    @staticmethod
    def get_kurtosis_z():
        """
        Kurtosis is a measure of the "tailedness" of the probability
        distribution, Z axis.
        https://en.wikipedia.org/wiki/Kurtosis
        :return: Kurtosis, no units.
        """
        return 1.5 + random.random()

    @staticmethod
    def get_kurtosis_x():
        """
        Kurtosis is a measure of the "tailedness" of the probability
        distribution, X axis.
        https://en.wikipedia.org/wiki/Kurtosis
        :return: Kurtosis, no units.
        """
        return 0.5 + random.random()

    @staticmethod
    def get_crest_acceleration_z():
        """
        Crest is a measure of a waveform showing the ratio of peak values
        to the effective value. In other words, crest factor indicates how
        extreme the peaks are in a waveform.  Crest factor 1 indicates no
        peaks, such as direct current. Z axis.
        https://en.wikipedia.org/wiki/Crest_factor
        :return: Crest acceleration factor in peak / RMS.
        """
        return 1.5 + random.random()

    @staticmethod
    def get_crest_acceleration_x():
        """
        Crest is a measure of a waveform showing the ratio of peak values
        to the effective value. In other words, crest factor indicates how
        extreme the peaks are in a waveform.  Crest factor 1 indicates no
        peaks, such as direct current. X axis.
        https://en.wikipedia.org/wiki/Crest_factor
        :return: Crest acceleration factor in peak / RMS.
        """
        return 0.5 + random.random()

    @staticmethod
    def get_peak_velocity_z():
        """
        Get peak vibration velocity, Z axis.
        :return: Vibration velocity in inches per second.
        """
        return 10 + 2.0 * random.random()

    @staticmethod
    def get_peak_velocity_x():
        """
        Get root mean square vibration velocity, X axis.
        :return: Vibration velocity in inches per second.
        """
        return 12 + 2.0 * random.random()

    @staticmethod
    def get_hf_rms_acceleration_z():
        """
        Get high-frequency root mean square acceleration, Z axis.
        :return: Acceleration in g.
        """
        return 0.2 + 0.1 * random.random()

    @staticmethod
    def get_hf_rms_acceleration_x():
        """
        Get high-frequency root mean square acceleration, X axis.
        :return: Acceleration in g.
        """
        return 0.3 + 0.1 * random.random()


class QM42VT2EmulatedDeviceIsoThreshRedAlarms(QM42VT2EmulatedDevice):
    """
    Banner qm42vt2 emulated data reader for creating sensor data
    resulting in Iso Thresholds alarms of "Red" severity.
    """

    type_key = 'qm42vt2_emulated_iso_red_alarm'

    @staticmethod
    def get_rms_velocity_x():
        """
        Get root mean square vibration velocity, X axis.

        rms_velocity_x threshold for "red" alarms: 0.37

        :returns: Vibration velocity in inches per second.
        """
        return 0.37 + 0.05 * random.random()


class QM42VT2EmulatedDeviceIsoThreshYellowAlarms(QM42VT2EmulatedDevice):
    """
    Banner qm42vt2 emulated data reader for creating sensor data
    resulting in Iso Thresholds alarms of "Orange"/"Yellow" severity.
    """

    type_key = 'qm42vt2_emulated_iso_yellow_alarm'

    @staticmethod
    def get_rms_velocity_x():
        """
        Get root mean square vibration velocity, X axis.

        rms_velocity_x threshold for "orange"/"yellow" alarms:
        - 0.3 (assets with higher threshold)
        - 0.18 (assets with lower threshold)

        :return: Vibration velocity in inches per second.
        """
        return 0.18 + 0.04 * random.random()


class QM42VT2EmulatedDeviceMTDAlarms(QM42VT2EmulatedDevice):
    """
    Banner qm42vt2 emulated data reader which creates
    sensor data that produces Mode Transition Detection (MTD) alarms.
    There is only yellow alarms currently available for MTD analytic.
    """

    type_key = 'qm42vt2_emulated_mtd_alarm'

    @staticmethod
    def get_rms_acceleration_x():
        """
        Get rms_acceleration_x
        rms_acceleration_x is used by conveyor status to determine
        on/off status of a particular conveyor. this will be most
        effective if this sensor is placed on a Motor equipment type
        the modulus function will cycle values between
        0.01 and 0.03. so every fourth minute will have
        a value of 0.03 (ON) then the next three minutes will
        have a vlaue of 0.01 (OFF).
        This should give us ~14-15 ON/OFF transitions every hour
        :return: Acceleration in g.
        """

        if datetime.now().minute % 4:
            distribution_center = 0.03
        else:
            distribution_center = 0.01

        return distribution_center + 0.004 * random.random()


class QM42VT2EmulatedDeviceMLAlarms(QM42VT2EmulatedDevice):
    """
    Banner qm42vt2 emulated data reader which creates
    sensor data that produces Machine Learning alarms.
    The logic behind this is to make the data respond to the
    time of day and move in ways different than what the
    data generated by the standard EmulatedDevice does
    It needs to override the functions for tags that
    are used as inputs into the regression models.
    """

    type_key = 'qm42vt2_emulated_ml_alarm'

    @staticmethod
    def get_rms_velocity_z():
        """
        Get rms_velocity_z forcing it to move
        in opposite manner from rms_velocity_z
        should trigger alarm

        :return: RMS Velocity in in/sec.
        """

        if datetime.now().minute % 4:
            distribution_center = 0.11
        else:
            distribution_center = 0.04

        return distribution_center + 0.04 * random.random()

    @staticmethod
    def get_rms_velocity_x():
        """
        Get rms_velocity_x

        :return: RMS Velocity in in/sec.
        """

        if datetime.now().minute % 4:
            distribution_center = 0.04
        else:
            distribution_center = 0.11

        return distribution_center + 0.04 * random.random()

    @staticmethod
    def get_kurtosis_z():
        """
        Kurtosis is a measure of the "tailedness" of the probability
        distribution, Z axis.
        https://en.wikipedia.org/wiki/Kurtosis
        :return: Kurtosis, no units.
        """
        return 0.90 + random.random()

    @staticmethod
    def get_kurtosis_x():
        """
        Kurtosis is a measure of the "tailedness" of the probability
        distribution, X axis.
        https://en.wikipedia.org/wiki/Kurtosis
        Overwriting to produce more strange data
        :return: Kurtosis, no units.
        """
        return 0.15 + random.random()

    @staticmethod
    def get_temperature():
        """
        Get temperature.
        :return: Temperature in Fahrenheit.
        """
        return 85 + 25.0 * random.random()
