"""
Contains all available device types and device management classes
"""
from typing import Type, Dict

from utils.general import import_attr
from .base import BaseDevice


_DEVICE_MAP: Dict[str, Type[BaseDevice]] = {}


ERROR_DEVICE_KEY = 'error'
QM42VT2_DEVICE_KEY = 'qm42vt2'
M12FTH3Q_DEVICE_KEY = 'm12fth3q'
QM42VT2_TCP_DEVICE_KEY = 'qm42vt2_tcp'
M12FTH3Q_TCP_DEVICE_KEY = 'm12fth3q_tcp'
CSV_PLC_DEVICE_KEY = 'csv_plc'
QM42VT2_EMULATED_DEVICE_KEY = 'qm42vt2_emulated'
M12FTH3Q_EMULATED_DEVICE_KEY = 'm12fth3q_emulated'
CSV_PLC_EMULATED_DEVICE_KEY = 'csv_plc_emulated'
# emulated alarms
QM42VT2_EMULATED_ML_ALARM_DEVICE_KEY = 'qm42vt2_emulated_ml_alarm'
QM42VT2_EMULATED_MTD_ALARM_DEVICE_KEY = 'qm42vt2_emulated_mtd_alarm'
QM42VT2_EMULATED_ISO_RED_ALARM_DEVICE_KEY = 'qm42vt2_emulated_iso_red_alarm'
QM42VT2_EMULATED_ISO_YELLOW_ALARM_DEVICE_KEY = 'qm42vt2_emulated_iso_yellow_alarm'


AVAILABLE_DEVICES = {
    QM42VT2_DEVICE_KEY: 'device.rs485.modbus.qm42vt2.QM42VT2Device',
    M12FTH3Q_DEVICE_KEY: 'device.rs485.modbus.m12fth3q.M12FTH3QDevice',
    QM42VT2_TCP_DEVICE_KEY: 'device.modbus_tcp.qm42vt2_tcp.QM42VT2TCPDevice',
    M12FTH3Q_TCP_DEVICE_KEY: 'device.modbus_tcp.m12fth3q_tcp.M12FTH3QTCPDevice',
    CSV_PLC_DEVICE_KEY: 'device.ethernet.cip.csv_plc.CSVPLCDevice',
    QM42VT2_EMULATED_DEVICE_KEY: 'device.emulated.qm42vt2.QM42VT2EmulatedDevice',
    M12FTH3Q_EMULATED_DEVICE_KEY: 'device.emulated.m12fth3q.M12FTH3QEmulatedDevice',
    CSV_PLC_EMULATED_DEVICE_KEY: 'device.emulated.csv_plc.CSVPLCEmulatedDevice',
    QM42VT2_EMULATED_ML_ALARM_DEVICE_KEY: 'device.emulated.qm42vt2.QM42VT2EmulatedDeviceMLAlarms',
    QM42VT2_EMULATED_MTD_ALARM_DEVICE_KEY: 'device.emulated.qm42vt2.QM42VT2EmulatedDeviceMTDAlarms',
    QM42VT2_EMULATED_ISO_RED_ALARM_DEVICE_KEY: 'device.emulated.qm42vt2.QM42VT2EmulatedDeviceIsoThreshRedAlarms',
    QM42VT2_EMULATED_ISO_YELLOW_ALARM_DEVICE_KEY: 'device.emulated.qm42vt2.QM42VT2EmulatedDeviceIsoThreshYellowAlarms'

}

DEVICE_KEYS = (ERROR_DEVICE_KEY,) + tuple(AVAILABLE_DEVICES.keys())


def get_device_cls(device_key: str) -> Type[BaseDevice]:
    """
    Get device class based on it's name
    :param device_key: type of device
    :return: Device class
    """
    if device_key not in _DEVICE_MAP:
        device_cls = import_attr(AVAILABLE_DEVICES[device_key])
        _DEVICE_MAP[device_key] = device_cls

    return _DEVICE_MAP[device_key]
