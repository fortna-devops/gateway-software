"""
    Port Manager class
    """
from typing import List, Set

from schema import Schema, And, Or, Regex

from manager import BaseManager
from utils import contained_by
from . import AVAILABLE_DEVICES
from .device_iterator import DeviceIterator


__all__ = ['PortManager']


class PortManager(BaseManager):
    """
    Port Manager class

    Holds all available port configurations
    """

    _validation_schema = Schema({
        Or('enip', 'modbus_tcp', Regex(r'^/dev/tty.+$')): [{
            'type': And(str, contained_by(AVAILABLE_DEVICES)),
            'interval': And(float, lambda n: n >= 0.1),
            'options': dict,
            'devices': [And(str, len)],
        }]
    })

    def __init__(self, port_schema: dict):
        port_schema = self._validate(port_schema)
        self._iterators = [DeviceIterator(p, s) for p, s in port_schema.items()]

    def get_unique_device_keys(self) -> Set[str]:
        """
        Unique set of used device keys
        :return: Unique set of used device keys
        """
        device_keys = set()
        for iterator in self._iterators:
            for device_type, _ in iterator.iter_devices():
                device_keys.add(device_type)

        return device_keys

    @property
    def device_iterators(self) -> List[DeviceIterator]:
        """
        Available device iterators
        :return: device iterators
        """
        return self._iterators
