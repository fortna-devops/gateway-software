"""
Device Iterator class
"""

from collections import defaultdict
from typing import Iterable, Tuple

from device import BaseDevice
from . import AVAILABLE_DEVICES, get_device_cls
from .error import DeviceError


__all__ = ['DeviceIterator']


class DeviceIterator():
    """
    Device Iterator class

    Allows to iterate over all available devices on this port
    """

    def __init__(self, port, port_schema):
        self._port = port
        self._device_map = defaultdict(list)

        for definition in port_schema:
            device_key = definition['type']
            read_interval = definition['interval']
            device_options = definition.get('options', {})
            devices = definition['devices']

            for device_id in devices:
                if device_key not in AVAILABLE_DEVICES:
                    raise DeviceError(f'Unknown device type: "{device_key}"')

                device_cls = get_device_cls(device_key)
                device = device_cls(port, device_id, read_interval, **device_options)
                self._device_map[device_key].append(device)

    @property
    def port(self):
        """
        Port that is used
        :return: port
        """
        return self._port

    def iter_devices(self) -> Iterable[Tuple[str, BaseDevice]]:
        """
        Iterates over all available devices on this port
        :return: device type, Device instance
        """
        for device_type, devices in self._device_map.items():
            for device in devices:
                yield device_type, device
