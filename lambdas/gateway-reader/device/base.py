"""
Device interface
"""

import logging
import time

from abc import ABC, abstractmethod
from typing import List, Any

__all__ = ['BaseDevice']


logger = logging.getLogger()


class BaseDevice(ABC):
    """
    Device interface
    """

    def __init__(self, port, address, read_interval, **_options):
        self._port = port
        self._address = address
        self._read_interval = read_interval
        self._last_read_time = time.time()

    @property
    @abstractmethod
    def type_key(self) -> str:
        """
        device type key
        :return: device key
        """
        return ''

    @property
    def port(self) -> str:
        """
        device port to be used
        :return: device port
        """
        return self._port

    @property
    def address(self) -> str:
        """
        device address to be used
        :return: device address
        """
        return self._address

    @property
    def read_interval(self) -> float:
        """
        interval to read device
        :return: reading interval
        """
        return self._read_interval

    @property
    def last_read_time(self) -> float:
        """
        timestamp of last device read attempt
        :return: timestamp
        """
        return self._last_read_time

    @abstractmethod
    def _read(self) -> List:
        """
        read data from device
        :return: data from device
        """
        return []

    @staticmethod
    def _normalize(raw_data: List[Any]) -> List[Any]:
        """
        normalize data if needed
        :param raw_data: data read from device
        :return: normalized data
        """
        return raw_data

    def read(self):
        """
        sets read time and read data from device
        :return: data from device
        """
        self._last_read_time = time.time()
        return self._read()

    def __repr__(self):
        type_name = type(self).__name__
        arg_strings = []
        for name in 'type_key', 'port', 'address', 'read_interval', 'last_read_time':
            arg_strings.append('%s=%r' % (name, getattr(self, name)))
        return '%s(%s)' % (type_name, ', '.join(arg_strings))
