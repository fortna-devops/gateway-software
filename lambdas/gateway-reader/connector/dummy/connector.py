"""
Dummy Connector
"""

import logging
from typing import List

from message import Message
from table_schema.base import BaseSchema
from ..base import BaseConnector


__all__ = ['Connector']


logger = logging.getLogger()


class Connector(BaseConnector):
    """
    Dummy connector that do not send data, just implements interface
    """

    def send(self, messages: List[Message], encoder: BaseSchema) -> int:
        """
        Emulates message send
        :param messages: list of 'Message'
        :param encoder: 'Message' encoding class
        :return: sent messages count
        """
        return len(messages)

    def write(self, key: str, data: bytes):
        """
        Emulates data writing by key
        :param key: path to write
        :param data: data to write
        """
