"""
S3 connector class
"""

import logging
import itertools
from typing import List

import pyarrow.parquet as pq
import s3fs

from message import Message
from table_schema.base import BaseSchema
from ..base import BaseConnector


__all__ = ['Connector']


logger = logging.getLogger()


class Connector(BaseConnector):
    """
    Connector implementation for S3
    """

    config_kwargs = {'retries': {'max_attempts': 0}}

    def __init__(self, site, bucket_name, profile_name=None, region_name=None):
        super(Connector, self).__init__(site=site)
        self._bucket_name = bucket_name
        self._fs = s3fs.S3FileSystem(profile_name=profile_name, region_name=region_name,
                                     config_kwargs=self.config_kwargs)

    def _send_part(self, messages: List[Message], date: str, encoder: BaseSchema) -> bool:
        """
        Send a piece of data for 'date'
        :param messages: list of 'Message' with specific 'date'
        :param date: date of 'messages'
        :param encoder: 'Message' encoding class
        :return: is 'messages' sent successfully
        """
        path = '/'.join((self._bucket_name, self._site, encoder.name, date))
        data = encoder.encode(messages)
        try:
            pq.write_to_dataset(data, path, filesystem=self._fs, compression='gzip')
        except BaseException as e: # pylint: disable=W0703
            logger.error(e)
            return False
        return True

    def send(self, messages: List[Message], encoder: BaseSchema) -> int:
        """
        Send 'messages' to S3 bucket
        :param messages: list of 'Message' to send
        :param encoder: 'Message' encoding class
        :return: sent messages count
        """
        sent_count = 0
        for key, group in itertools.groupby(messages, self._get_timestamp):
            data = list(group)
            date = key.strftime("date=%Y-%m-%d")
            if not self._send_part(data, date, encoder):
                break
            sent_count += len(data)
        return sent_count

    def write(self, key: str, data: bytes):
        """
        Write data by key to S3
        :param key: s3 key to write
        :param data: data to write
        """
        with self._fs.open(f'{self._bucket_name}/{self._site}/{key}', 'wb') as f:
            f.write(data)
