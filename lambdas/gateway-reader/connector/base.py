"""
Base class for connector
"""

import datetime
from abc import ABC, abstractmethod
from typing import List

from message import Message
from table_schema import BaseSchema

__all__ = ['BaseConnector']


class BaseConnector(ABC):
    """
    Base class for connector
    """

    def __init__(self, site):
        self._site = site

    @staticmethod
    def _get_timestamp(message):
        timestamp = datetime.datetime.utcfromtimestamp(message.timestamp / 1000)
        return timestamp.replace(second=0, microsecond=0)

    @abstractmethod
    def send(self, messages: List[Message], encoder: BaseSchema) -> int:
        """
        Send 'messages' to S3 bucket
        :param messages: list of 'Message' to send
        :param encoder: 'Message' encoding class
        :return: number of sent messages.
        """
        return 0

    @abstractmethod
    def write(self, key: str, data: bytes):
        """
        Writes data to specified key.
        """
