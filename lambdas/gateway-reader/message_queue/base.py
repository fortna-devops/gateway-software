"""
Base Message Queue class
"""

from abc import ABC, abstractmethod


__all__ = ['BaseMessageQueue']


class BaseMessageQueue(ABC):
    """
    Base class for message queue
    """

    @abstractmethod
    def is_empty(self):
        """
        Check if queue is empty.

        :return boolean value.
        """

    @abstractmethod
    def push(self, message):
        """
        Push message to queue.

        :param message: Message to push.
        """

    @abstractmethod
    def get_first(self, num=1):
        """
        Get specified number of messages from the queue.

        :param num: Number of messages to get.
        :returns: tuple (num_leftover_messages, message_list)
            WHERE
            num_leftover_messages is the number of leftover messages
            message_list is the list of messages
        """

    @abstractmethod
    def remove_first(self, num=1):
        """
        Remove specified number of messages from the queue.

        :param num: Number of messages to be removed.
        :return: List of removed messages.
        """

    @abstractmethod
    def clear(self):
        """
        Remove all data from cache.
        """
