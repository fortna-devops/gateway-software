"""
Persistent cache class
"""

import os
import shutil
import logging
from threading import Lock

from diskcache import Deque

from message_queue import BaseMessageQueue
from ..error import QueueError


__all__ = ['PersistMessageQueue']


logger = logging.getLogger()


class PersistMessageQueue(BaseMessageQueue):
    """
    Hold messages in local cache on disk
    """

    def __init__(self, cache_dir, clean_start=False):
        self.__cache_dir = cache_dir
        if clean_start:
            shutil.rmtree(cache_dir, True)

        if os.path.exists(self.__cache_dir) and not os.path.isdir(self.__cache_dir):
            raise QueueError(f'"{self.__cache_dir}" exists but is not a directory.')

        if not os.path.exists(self.__cache_dir):
            os.makedirs(self.__cache_dir)

        self._mutex = Lock()
        self._deque = Deque(directory=self.__cache_dir)

    def is_empty(self):
        return len(self._deque) == 0

    def push(self, message):
        logger.debug(f'Got new message: {message}')
        self._deque.append(message)

    def get_first(self, num=1):
        with self._mutex:
            deque_len = len(self._deque)
            num_leftover_messages = max(0, deque_len - num)
            message_list = [self._deque[i] for i in range(num) if i < deque_len]
            return num_leftover_messages, message_list

    def remove_first(self, num=1):
        with self._mutex:
            remove_message_list = [self._deque.popleft() for _ in range(num)]
            return remove_message_list

    def clear(self):
        self._deque.clear()
