# To reduce the deployment package size all dependencies should be installed
# during gateway preparation step.
# Anyway, it's still possible to add requirements here and they will be added
# to the deployment package.
