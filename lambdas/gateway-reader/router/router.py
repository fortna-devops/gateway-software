"""
Router class
"""

import logging
from typing import Optional

from message import Message

__all__ = ['Router']


logger = logging.getLogger()


class Router():
    """
    Redirect messages to the corresponding queue
    """

    def __init__(self, routes: Optional[dict]):
        self._routes = routes or {}

    def push(self, message: Message):
        """
        Push message to the corresponding queue
        :param message: message to push
        :return:
        """
        if message.m_type not in self._routes:
            logger.error(f'Unknown message type: "{message.m_type}"')
            return

        self._routes[message.m_type].push(message)
