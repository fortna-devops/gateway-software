"""
General purpose utility methods
"""

import importlib
import logging

from functools import partial
from operator import contains
from typing import Any, Iterable, Callable

__all__ = ['contained_by', 'import_attr']


logger = logging.getLogger()


def contained_by(iterable: Iterable[Any]) -> Callable:
    """
    Check that iterable contains element
    :param iterable: iterable to check
    :return: partial function
    """
    return partial(contains, iterable)


def import_attr(path: str) -> Any:
    """
    Imports module and gets attribute from it by name
    :param path: path to class e.g. 'module.submodule.attribute'
    :return: attribute from module
    """
    logger.debug(f'Loading class "{path}"')
    module_name, class_name = path.rsplit(".", 1)
    module = importlib.import_module(module_name)
    cls = getattr(module, class_name)
    return cls
