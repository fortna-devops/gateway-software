"""
Mapping for data types
"""

FLOAT_TYPE_KEY = 'float'
INT_TYPE_KEY = 'int'
STRING_TYPE_KEY = 'string'
TIMESTAMP_TYPE_KEY = 'timestamp'


PY_TYPE_MAP = {
    FLOAT_TYPE_KEY: float,
    INT_TYPE_KEY: int,
    STRING_TYPE_KEY: str,
    TIMESTAMP_TYPE_KEY: int,
}
