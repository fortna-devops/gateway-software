"""
Utilities for argparse lib
"""

import json
from argparse import Action, ArgumentError


__all__ = ['min_max_fabric', 'ParseJSONAction']


def min_max_fabric(_min=None, _max=None):
    """
    Fabric for ranged values action.
    """

    class MinMaxAction(Action):
        """
        Provide limits for value.
        """

        def _validate(self, value):
            if _min is not None and value < _min:
                msg = 'value %s should be greater than %s' % (value, _min)
                raise ArgumentError(self, msg)

            if _max is not None and value > _max:
                msg = 'value %s should be less than %s' % (value, _max)
                raise ArgumentError(self, msg)

        def __call__(self, parser, namespace, values, option_string=None):
            self._validate(values)
            setattr(namespace, self.dest, values)

    return MinMaxAction


class ParseJSONAction(Action):
    """
    Parse json from arguments.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        try:
            values = json.loads(values.strip("'"))
        except ValueError:
            raise ArgumentError(self, 'can\'t parse JSON')

        setattr(namespace, self.dest, values)
