"""
Utility methods for logging
"""
import logging


__all__ = ['configure_logs']


CPPPO_LOGGERS = ['enip', 'cpppo', 'network']
LOCAL_LOG_FORMAT = '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'


def _disable_cpppo_log():
    """
    cpppo custom log levels ("normal" and "detail") are incompatible with AWS
    greengrass custom log handlers. Also cpppo writes tons of logs on INFO
    level, so we need to set log level to WARNING.
    """
    for logger_name in CPPPO_LOGGERS:
        logging.getLogger(logger_name).setLevel(logging.WARNING)


def _configure_local_logs(log_level):
    """
    Configure logging for non-greengrass runs (on local machine)
    """
    logging.basicConfig(level=log_level, format=LOCAL_LOG_FORMAT)


def configure_logs(local=False, log_level=logging.WARNING):
    """
    Disable cpppo custom logging and configure local logging
    """
    _disable_cpppo_log()

    if local:
        _configure_local_logs(log_level)
