#!/usr/bin/env python
"""
Entrypoint for the gateway reader
"""

import logging

import configargparse

from utils import ParseJSONAction, configure_logs
from gateway import Gateway

logger = logging.getLogger()


PARSER = configargparse.ArgumentParser(default_config_files=['config.ini'])
PARSER.add_argument('-c', '--config', is_config_file=True, help='path to config file')

PARSER.add_argument('--gateway', env_var='GATEWAY', required=True, dest='name',
                    help='name of gateway')
PARSER.add_argument('--site', env_var='SITE', required=True, help='site id to store data')
PARSER.add_argument('--connector', env_var='CONNECTOR', default='connector.s3.Connector')
PARSER.add_argument('--connector_kwargs', env_var='CONNECTOR_KWARGS', action=ParseJSONAction,
                    default=None)
PARSER.add_argument('--device_schema', env_var='DEVICE_SCHEMA',
                    action=ParseJSONAction, required=True,
                    help='json-like device definition '
                         '{"/dev/ttyS1": [{"interval": 1, "type": "qm42vt2", "options": {}, "devices": [1,2,3,4]}]}')
PARSER.add_argument('--mapping_schema', env_var='MAPPING_SCHEMA',
                    action=ParseJSONAction, default=None,
                    help='json-like mapping definition (default is None) '
                         '{"csv_plc": {"table_key": "plc", "mapping": [{"index": 3, "type": "int"}]}}')
PARSER.add_argument('--table_schema', env_var='TABLE_SCHEMA',
                    action=ParseJSONAction, default=None,
                    help='json-like table definition (default is None) '
                         '{"plc": [{"name": "torque", "type": "int"}]}}')
PARSER.add_argument('--send_interval', env_var='SEND_INTERVAL', default=60,
                    type=int, help='data send interval in seconds (default is 60)')
PARSER.add_argument('--heartbeat_interval', env_var='HEARTBEAT_INTERVAL', default=60,
                    type=int, help='heartbeat send interval in seconds (default is 60)')
PARSER.add_argument('--cache_dir', env_var='CACHE_DIR', default='/var/cache/gateway_cache',
                    help='path to cache dir (default is "/var/cache/gateway_cache")')
PARSER.add_argument('--clean_start', env_var='CLEAN_START', action='store_true',
                    help='remove all data from cache before start')

LOG_GROUP = PARSER.add_mutually_exclusive_group()
LOG_GROUP.add_argument('-d', '--debug', action="store_const",
                       help="print debug info", dest="log_level", const=logging.DEBUG,
                       default=logging.INFO)
LOG_GROUP.add_argument('-s', '--silent', help="be silent", action="store_const", dest="log_level",
                       const=logging.CRITICAL)

ARGS, _ = PARSER.parse_known_args()
ARGS = vars(ARGS)

# remove config file from args
ARGS.pop('config')

# determine the environment
LOCAL = __name__ == '__main__'

# configure logging
configure_logs(LOCAL, ARGS.pop('log_level'))

# initializing the Gateway
GATEWAY = Gateway(**ARGS)

# running gateway
GATEWAY.start()


def handler(_event, _context):
    """
    This is a dummy handler for AWS and will not be invoked.
    Instead the code above will be executed in an infinite loop.
    """
    return
