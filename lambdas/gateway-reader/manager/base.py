"""
Interface for manager class
"""

from abc import ABC, abstractmethod
from typing import Any

from schema import Schema

__all__ = ['BaseManager']


class BaseManager(ABC):
    """
    Interface for manager class
    """

    @property
    @abstractmethod
    def _validation_schema(self) -> Schema:
        """
        Validation schema for config
        :return: validation schema
        """

    def _validate(self, data: Any) -> Any:
        """
        Validates config
        :param data: config to validate
        :return: validated config
        """
        return self._validation_schema.validate(data)
