"""
Base class for readers
"""

import logging

from worker import BaseWorker


__all__ = ['BaseReader']


logger = logging.getLogger()


class BaseReader(BaseWorker):  # pylint: disable=W0223
    """
    Base class for readers
    """

    def __init__(self, gateway_name, device_manager, message_queue, data_mapper,
                 thread_name='ReaderWorker'):
        super(BaseReader, self).__init__(thread_name=thread_name)
        self._gateway_name = gateway_name
        self._device_manager = device_manager
        self._message_queue = message_queue
        self._data_mapper = data_mapper
