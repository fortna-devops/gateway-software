"""
Consecutive reader implementation
"""
import logging
import time

from device import ERROR_DEVICE_KEY
from message import Message
from .base import BaseReader

__all__ = ['ConsecutiveReader']


logger = logging.getLogger()


class ConsecutiveReader(BaseReader):
    """
    Consecutive reader implementation
    """

    def _run(self, *args, **kwargs):
        """
        iterates over devices and read them consecutively
        :param args:
        :param kwargs:
        :return:
        """
        for _table, device in self._device_manager.iter_devices():
            with self._log_error():
                if time.time() - device.last_read_time < device.read_interval:
                    continue

                port = str(device.port)
                address = str(device.address)

                timestamp = int(round(time.time() * 1000))
                payload = [timestamp, self._gateway_name, port, address]

                try:
                    logger.debug(f'Reading device {device}')
                    data = device.read()
                    key = device.type_key
                except BaseException as e: # pylint: disable=W0703
                    msg = f'Can\'t read port: "{port}" address: "{address}" ' \
                          f'error_type: {e.__class__.__name__} message: "{e}"'
                    logger.error(msg)
                    logger.debug("", exc_info=e)
                    data = [msg]
                    key = ERROR_DEVICE_KEY

                table_key, mapped_data = self._data_mapper.apply(key, data)
                payload.extend(mapped_data)

                message = Message(table_key, payload, timestamp)
                logger.debug(f'Push message {message}')
                self._message_queue.push(message)
                logger.debug('Message pushed')
