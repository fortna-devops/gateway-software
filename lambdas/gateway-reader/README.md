# Predictive Maintenance gateway source code
Gateway is based on Dell Edge Gateway 5100 with Ubuntu OS. It also can be run
on any laptop with Python in sensor emulation mode. In this mode data will be
generated with pseudo random number generator.

# Installation
```
pip install -r requirements-dev.txt
```

# Running
```
pyhon main.py
```

## Usage of `main.py`
```
usage: main.py [-h] [-c CONFIG] --gateway NAME --site SITE
               [--connector CONNECTOR] [--connector_kwargs CONNECTOR_KWARGS]
               --device_schema DEVICE_SCHEMA [--mapping_schema MAPPING_SCHEMA]
               [--table_schema TABLE_SCHEMA] [--send_interval SEND_INTERVAL]
               [--heartbeat_interval HEARTBEAT_INTERVAL]
               [--cache_dir CACHE_DIR] [--clean_start] [-d | -s]

Args that start with '--' (eg. --gateway) can also be set in a config file
(config.ini or specified via -c). Config file syntax allows: key=value,
flag=true, stuff=[a,b,c] (for details, see syntax at https://goo.gl/R74nmi).
If an arg is specified in more than one place, then commandline values
override environment variables which override config file values which
override defaults.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        path to config file
  --gateway NAME        name of gateway [env var: GATEWAY]
  --site SITE           site id to store data [env var: SITE]
  --connector CONNECTOR
                        [env var: CONNECTOR]
  --connector_kwargs CONNECTOR_KWARGS
                        [env var: CONNECTOR_KWARGS]
  --device_schema DEVICE_SCHEMA
                        json-like device definition {"/dev/ttyS1":
                        [{"interval": 1, "type": "qm42vt2", "options": {},
                        "devices": [1,2,3,4]}]} [env var: DEVICE_SCHEMA]
  --mapping_schema MAPPING_SCHEMA
                        json-like mapping definition (default is None)
                        {"csv_plc": {"table_key": "plc", "mapping": [{"index":
                        3, "type": "int"}]}} [env var: MAPPING_SCHEMA]
  --table_schema TABLE_SCHEMA
                        json-like table definition (default is None) {"plc":
                        [{"name": "torque", "type": "int"}]}} [env var:
                        TABLE_SCHEMA]
  --send_interval SEND_INTERVAL
                        data send interval in seconds (default is 60) [env
                        var: SEND_INTERVAL]
  --heartbeat_interval HEARTBEAT_INTERVAL
                        heartbeat send interval in seconds (default is 60)
                        [env var: HEARTBEAT_INTERVAL]
  --cache_dir CACHE_DIR
                        path to cache dir (default is
                        "/var/cache/gateway_cache") [env var: CACHE_DIR]
  --clean_start         remove all data from cache before start [env var:
                        CLEAN_START]
  -d, --debug           print debug info
  -s, --silent          be silent
```

## Usage of `config.ini`
Example of config file can be found in [example_config.ini](example_config.ini)


# Tests
Project has tests. To run them execute `tox`.
