"""
Table Manager class
"""

from operator import itemgetter
from typing import Iterable, Optional, Dict

from schema import Schema, And, Or

from manager import BaseManager
from table_schema import BaseSchema
from utils import contained_by, PY_TYPE_MAP
from . import AVAILABLE_SCHEMAS, DEFAULT_TABLE_KEYS, get_schema_cls


__all__ = ['TableManager']


class TableManager(BaseManager):
    """
    Table Manager class
    Holds all known table schemas
    """

    _validation_schema = Schema({
        And(str, contained_by(AVAILABLE_SCHEMAS)): Or(None, [{
            'type': And(str, contained_by(PY_TYPE_MAP)),
            'name': And(str, len)
        }])
    })

    key_func = itemgetter('name')

    def __init__(self, table_keys: Iterable[str], table_schema: Optional[dict] = None):
        if not isinstance(table_keys, set):
            table_keys = set(table_keys)
        table_keys.update(DEFAULT_TABLE_KEYS)

        base_schema = {k: None for k in table_keys}

        if table_schema is None:
            table_schema = {}

        validated_schema = self._validate(dict(base_schema, **table_schema))

        self._schemas: Dict[str, BaseSchema] = {}
        for table_name, schema_def in validated_schema.items():
            schema_cls = get_schema_cls(table_name)
            schema = schema_cls(schema_def)
            self._schemas[table_name] = schema

    @property
    def schemas(self) -> Dict[str, BaseSchema]:
        """
        Known schemas
        :return: Known schemas
        """
        return self._schemas
