"""
Schema interface
"""
from abc import ABC, abstractmethod
from typing import List, Any

from message import Message

__all__ = ['BaseSchema']


class BaseSchema(ABC):
    """
    Schema interface
    """

    def __init__(self, *args, **kwargs):
        pass

    @property
    @abstractmethod
    def table_key(self) -> str:
        """
        Table key
        :return: table key
        """
        return ''

    @abstractmethod
    def encode(self, messages: List[Message]) -> Any:
        """
        Encodes messages
        :param messages:
        :return:
        """
        return []

    @abstractmethod
    def _validate(self, message: Message):
        """
        Validates message
        :param message: message to validate
        """

    def _get_data(self, message: Message) -> Any:
        """
        Run validation for message and gets it's data
        :param message:
        :return:
        """
        self._validate(message)
        return message.data

    @property
    def name(self):
        """
        Table name, in this case it's equal to table key
        :return: table name
        """
        return self.table_key
