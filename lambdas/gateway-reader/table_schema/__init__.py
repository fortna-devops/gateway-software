"""
Contains all available table schemas and table management classes
"""

from typing import Type, Dict

from utils.general import import_attr
from .base import BaseSchema


_SCHEMA_MAP: Dict[str, Type[BaseSchema]] = {}


SENSOR_SCHEMA_KEY = 'sensor'
AMBIENT_ENV_SCHEMA_KEY = 'ambient_env'
PLC_SCHEMA_KEY = 'plc'
ERROR_SCHEMA_KEY = 'error'


AVAILABLE_SCHEMAS = {
    SENSOR_SCHEMA_KEY: 'table_schema.paruqet.sensor.SensorParquetSchema',
    AMBIENT_ENV_SCHEMA_KEY: 'table_schema.paruqet.ambient_env.AmbientEnvParquetSchema',
    PLC_SCHEMA_KEY: 'table_schema.paruqet.plc.PLCParquetSchema',
    ERROR_SCHEMA_KEY: 'table_schema.paruqet.error.ErrorParquetSchema'
}


DEFAULT_TABLE_KEYS = (ERROR_SCHEMA_KEY,)


def get_schema_cls(table_key: str) -> Type[BaseSchema]:
    """
    Get schema class based on it's name
    :param table_key: name of schema
    :return: Schema class
    """
    if table_key not in _SCHEMA_MAP:
        device_cls = import_attr(AVAILABLE_SCHEMAS[table_key])
        _SCHEMA_MAP[table_key] = device_cls

    return _SCHEMA_MAP[table_key]
