"""
Base Parquet schema class
"""

from abc import abstractmethod
from typing import List

import pyarrow as pa

from message import Message
from utils.data_types import PY_TYPE_MAP, FLOAT_TYPE_KEY, INT_TYPE_KEY, STRING_TYPE_KEY, \
    TIMESTAMP_TYPE_KEY
from ..base import BaseSchema


__all__ = ['BaseParquetSchema', 'ParquetSchemaError']


PA_TYPE_MAP = {
    FLOAT_TYPE_KEY: pa.float64(),
    INT_TYPE_KEY: pa.int64(),
    STRING_TYPE_KEY: pa.string(),
    TIMESTAMP_TYPE_KEY: pa.timestamp('ms', 'UTC'),
}


class ParquetSchemaError(Exception):
    """
    ParquetSchemaError
    """


class BaseParquetSchema(BaseSchema):
    """
    Schema implementation for parquet type
    """

    _base_fields = (
        {'name': 'timestamp', 'type': TIMESTAMP_TYPE_KEY},
        {'name': 'gateway', 'type': STRING_TYPE_KEY},
        {'name': 'port', 'type': STRING_TYPE_KEY},
        {'name': 'sensor', 'type': STRING_TYPE_KEY},
    )

    def __init__(self, fields=None):
        super(BaseParquetSchema, self).__init__()
        if not fields:
            fields = self._fields
        schema_def = self._base_fields + tuple(fields)
        self._py_schema = []
        _schema = []
        for field_def in schema_def:
            field_name = field_def['name']
            field_type = field_def['type']
            if field_type not in PY_TYPE_MAP or field_type not in PA_TYPE_MAP:
                raise ParquetSchemaError(f'Unsupported parquet field {field_def}')

            self._py_schema.append(PY_TYPE_MAP[field_type])
            _schema.append(pa.field(field_name, PA_TYPE_MAP[field_type]))

        self._schema = pa.schema(_schema)

    @property
    @abstractmethod
    def _fields(self):
        return ()

    def _validate(self, message: Message):
        """
        Validates message
        :param message: message to validate
        """
        data = message.data
        if len(data) != len(self._py_schema):
            raise ParquetSchemaError(f'Message length is not equal to the length of the schema for "{self.table_key}" table')

        for i, (py_type, cell) in enumerate(zip(self._py_schema, data)):
            if not isinstance(cell, py_type):
                raise ParquetSchemaError(f'Message has type mismatch in column {i} for "{self.table_key}" table')

    def encode(self, messages: List[Message]) -> pa.Table:
        """
        Encodes messages to certain format
        :param messages: list of 'Message'
        :return: table with data
        """
        data = [self._get_data(message) for message in messages]
        values = [pa.array(column) for column in zip(*data)]
        return pa.Table.from_arrays(arrays=values, schema=self._schema)
