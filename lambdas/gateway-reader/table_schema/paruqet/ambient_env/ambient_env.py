"""
Ambient environment schema class
"""

from utils.data_types import FLOAT_TYPE_KEY
from ..parquet import BaseParquetSchema


__all__ = ['AmbientEnvParquetSchema']


class AmbientEnvParquetSchema(BaseParquetSchema):
    """
    Ambient environment schema class
    Holds the schema for the 'ambient_env' table
    """

    table_key = 'ambient_env'

    _fields = (
        {'name': 'humidity', 'type': FLOAT_TYPE_KEY},
        {'name': 'temperature', 'type': FLOAT_TYPE_KEY},
    )
