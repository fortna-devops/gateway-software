"""
Error schema class
"""

from utils.data_types import STRING_TYPE_KEY
from ..parquet import BaseParquetSchema


__all__ = ['ErrorParquetSchema']


class ErrorParquetSchema(BaseParquetSchema):
    """
    Error schema class
    Holds the schema for the 'error' table
    """

    table_key = 'error'

    _fields = (
        {'name': 'error', 'type': STRING_TYPE_KEY},
    )
