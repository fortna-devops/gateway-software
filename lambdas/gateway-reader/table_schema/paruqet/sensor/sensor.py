"""
Sensor schema class
"""

from utils.data_types import FLOAT_TYPE_KEY
from ..parquet import BaseParquetSchema


__all__ = ['SensorParquetSchema']


class SensorParquetSchema(BaseParquetSchema):
    """
    Sensor schema class
    Holds the schema for the 'sensor' table
    """

    table_key = 'sensor'

    _fields = (
        {'name': 'rms_velocity_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'temperature', 'type': FLOAT_TYPE_KEY},
        {'name': 'rms_velocity_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_acceleration_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_acceleration_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_frequency_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_frequency_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'rms_acceleration_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'rms_acceleration_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'kurtosis_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'kurtosis_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'crest_acceleration_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'crest_acceleration_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_velocity_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'peak_velocity_x', 'type': FLOAT_TYPE_KEY},
        {'name': 'hf_rms_acceleration_z', 'type': FLOAT_TYPE_KEY},
        {'name': 'hf_rms_acceleration_x', 'type': FLOAT_TYPE_KEY},
    )
