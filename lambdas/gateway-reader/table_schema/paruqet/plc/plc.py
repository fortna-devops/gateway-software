"""
PLC schema class
"""

from utils.data_types import INT_TYPE_KEY, FLOAT_TYPE_KEY, STRING_TYPE_KEY
from ..parquet import BaseParquetSchema


__all__ = ['PLCParquetSchema']


class PLCParquetSchema(BaseParquetSchema):
    """
    PLC schema class
    Holds the schema for the 'plc' table
    """

    table_key = 'plc'

    _fields = (
        {'name': 'belt_speed', 'type': INT_TYPE_KEY},
        {'name': 'vfd_current', 'type': FLOAT_TYPE_KEY},
        {'name': 'vfd_frequency', 'type': FLOAT_TYPE_KEY},
        {'name': 'vfd_voltage', 'type': FLOAT_TYPE_KEY},
        {'name': 'vfd_fault_code', 'type': INT_TYPE_KEY},
        {'name': 'sorter_induct_rate', 'type': INT_TYPE_KEY},
        {'name': 'sorter_reject_rate', 'type': INT_TYPE_KEY},
        {'name': 'status', 'type': STRING_TYPE_KEY},
    )
