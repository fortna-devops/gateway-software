"""
Heartbeat class
"""

import logging
import time

from connector import BaseConnector
from worker import BaseWorker, sleep


__all__ = ['Heartbeat']


logger = logging.getLogger()


class Heartbeat(BaseWorker):
    """
    Heartbeat class
    Send heartbeat using connector
    """

    def __init__(self, connector: BaseConnector, send_interval: float,
                 timestamp_file: str = 'heartbeat.unixtimestamp',
                 thread_name: str = 'HeartbeatWorker'):
        self._connector = connector
        self._send_interval = send_interval
        self._timestamp_file = timestamp_file
        super(Heartbeat, self).__init__(thread_name=thread_name)

    def _run(self, *args, **kwargs):
        timestamp = str(int(round(time.time() * 1000)))
        logger.info(f'Sending timestamp "{timestamp}"')
        try:
            self._connector.write('heartbeat.unixtimestamp', timestamp.encode())
        except BaseException as e:  # pylint: disable=W0703
            logger.error(f'Failed to store timestamp: {e}')
        sleep(self._send_interval, self._shutdown_event)
