"""
Main class to handle all gateway reader activities
"""

import os
import signal
import logging

from reader.consecutive import ConsecutiveReader
from device.port_manager import PortManager
from message_queue.persist import PersistMessageQueue
from data_mapper.mapping_manager import MappingManager
from router import Router
from utils import import_attr
from writer import Writer
from heartbeat import Heartbeat
from table_schema.table_manager import TableManager
from worker.supervisor import Supervisor

logger = logging.getLogger()


class Gateway():
    """
    Gateway reads data and sends with connector.
    """

    terminate_signals = [signal.SIGINT, signal.SIGTERM]

    def __init__(self, name, site, device_schema, mapping_schema, table_schema, send_interval,
                 heartbeat_interval, cache_dir, connector, connector_kwargs=None,
                 clean_start=False):

        self._init_terminate()

        self._name = name
        self._site = site

        self._connector_cls = import_attr(connector)
        if connector_kwargs is None:
            connector_kwargs = {}
        self._connector_kwargs = connector_kwargs

        self._mapping_manager = MappingManager(mapping_schema)
        self._port_manager = PortManager(device_schema)
        self._table_manager = TableManager(self._get_tables_by_devices(), table_schema)

        self._router = None
        self._workers = []

        self._init_writers(send_interval, cache_dir, clean_start)
        self._init_readers()
        self._init_extras(heartbeat_interval)

        self._supervisor = Supervisor(self._workers)

    def _get_connector(self):
        return self._connector_cls(site=self._site, **self._connector_kwargs)

    def _init_writers(self, send_interval: float, cache_dir: str, clean_start: bool):
        queues = {}
        for table_key, table_schema in self._table_manager.schemas.items():
            cache_path = os.path.join(cache_dir, table_key)
            queue = PersistMessageQueue(cache_path, clean_start)
            queues[table_key] = queue
            writer = Writer(queue, self._get_connector(), table_schema, send_interval,
                            thread_name=f'WriterWorker_{table_key}')
            self._workers.append(writer)

        self._router = Router(queues)

    def _init_readers(self):
        for device_iterator in self._port_manager.device_iterators:
            port = device_iterator.port
            reader = ConsecutiveReader(self._name, device_iterator, self._router,
                                       self._mapping_manager, thread_name=f'ReaderWorker_{port}')
            self._workers.append(reader)

    def _init_extras(self, heartbeat_interval: float):
        # init heartbeat
        heartbeat = Heartbeat(self._get_connector(), heartbeat_interval)
        self._workers.append(heartbeat)

    def _init_terminate(self):
        for sig in self.terminate_signals:
            signal.signal(sig, self._signal_handler)

    def _get_tables_by_devices(self):
        devices_keys = self._port_manager.get_unique_device_keys()
        return self._mapping_manager.get_unique_table_keys(devices_keys)

    def _signal_handler(self, signum, _frame):
        """
        Warm shutdown.
        """
        logging.info(f'Got signal {signum}')
        self.stop()

    def start(self):
        """
        Start sender and readers, run infinite loop.
        """
        logger.info('Starting gateway...')
        self._supervisor.start()

    def stop(self):
        """
        Stop any activities and shutdown.
        """
        logger.info('Shutdown')
        self._supervisor.stop()
