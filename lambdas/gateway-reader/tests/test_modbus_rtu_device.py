"""
Tests for ModBusRTU devices
"""

import os
import time
import signal
import subprocess
import threading
import unittest
from abc import ABC, abstractmethod
from typing import List, Any

from pymodbus.framer.rtu_framer import ModbusRtuFramer
from pymodbus.server.sync import ModbusSerialServer
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from device.rs485.modbus.qm42vt2.qm42vt2 import QM42VT2Device
from device.rs485.modbus.m12fth3q.m12fth3q import M12FTH3QDevice


class BaseModBusRTUTest(unittest.TestCase, ABC):
    """
    Base class for ModBusRTU devices
    """

    sensor_id = 1
    timeout = .05
    baudrate = 19200

    @property
    @abstractmethod
    def server_port(self) -> str:
        """
        Port that will be used by emulation server
        :return: Port that will be used by emulation server
        """
        return ''

    @property
    @abstractmethod
    def client_port(self) -> str:
        """
        Port that will be used by device
        :return: Port that will be used by device
        """
        return ''

    @property
    @abstractmethod
    def starting_address(self) -> int:
        """
        Stat holding register address
        :return: Stat holding register address
        """
        return 0

    @property
    @abstractmethod
    def raw_data(self) -> List[int]:
        """
        Raw data that will be stored in holding registers
        :return: Raw data that will be stored in holding registers
        """
        return []

    @property
    @abstractmethod
    def expected_data(self) -> List[Any]:
        """
        Data that should be read from device
        :return: Data that should be read from device
        """
        return []

    def setUp(self):
        # start socat
        self._socat = subprocess.Popen([
            'socat',
            f'PTY,raw,echo=0,link={self.server_port}',
            f'PTY,raw,echo=0,link={self.client_port}'
        ])
        while not os.path.exists(self.server_port):
            time.sleep(.005)

        # init and start modbusRTU emulation server
        store = ModbusSlaveContext(
            hr=ModbusSequentialDataBlock(self.starting_address+1, self.raw_data)
        )
        context = ModbusServerContext(slaves=store, single=True)
        self._server = ModbusSerialServer(
            context=context, framer=ModbusRtuFramer, timeout=self.timeout,
            port=self.server_port, baudrate=self.baudrate
        )
        self._thread = threading.Thread(target=self._server.serve_forever, daemon=True)
        self._thread.start()

    def tearDown(self):
        self._server.server_close()
        self._thread.join()
        self._socat.send_signal(signal.SIGTERM)
        self._socat.wait()


class TestQM42VT2RTUDevice(BaseModBusRTUTest):
    """
    Tests for QM42VT2 over ModBusRTU
    """

    server_port = './ttyTestQM42VT2_0'
    client_port = './ttyTestQM42VT2_1'

    starting_address = 5200

    raw_data = [47, 119, 8307, 2837, 56, 142, 49, 38, 97, 97, 7, 7, 3076, 2936,
                4848, 3843, 66, 169, 79, 202, 10, 9]

    expected_data = [0.0047, 0.0119, 83.07, 28.37, 0.0056, 0.0142, 0.049, 0.038,
                     9.7, 9.7, 0.007, 0.007, 3.076, 2.936, 4.848, 3.843, 0.0066,
                     0.0169, 0.0079, 0.0202, 0.01, 0.009]

    def test_read(self):
        """
        Test read data
        :return:
        """
        device = QM42VT2Device(self.client_port, self.sensor_id, 1)
        data = device.read()
        self.assertListEqual(self.expected_data, data)


class TestM12FTH3QRTUDevice(BaseModBusRTUTest):
    """
    Tests for QM42VT2 over ModBusRTU
    """

    server_port = './ttyTestM12FTH3Q_0'
    client_port = './ttyTestM12FTH3Q_1'

    starting_address = 0000

    raw_data = [5100, 1661, 567]
    expected_data = [51.0, 83.05, 28.35]

    def test_read(self):
        """
        Test read data
        :return:
        """
        device = M12FTH3QDevice(self.client_port, self.sensor_id, 1)
        data = device.read()
        self.assertListEqual(self.expected_data, data)
