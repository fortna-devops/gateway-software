"""
Tests for message queue
"""

import os
import shutil
import unittest

from message_queue.persist import PersistMessageQueue


class TestPersistMessageQueue(unittest.TestCase):
    """
    Tests for message queue
    """
    cache_dir = '/tmp/test_gateway_cache'

    def setUp(self):
        os.makedirs(self.cache_dir)

    def tearDown(self):
        shutil.rmtree(self.cache_dir)

    @staticmethod
    def _get_data(num):
        return [f'test_{i}' for i in range(num)]

    def test_add_remove_get(self):
        """
        Test that queue can add, remove, and get messages
        :return:
        """
        obj_1, obj_2 = self._get_data(2)
        queue = PersistMessageQueue(self.cache_dir)
        queue.clear()
        queue.push(obj_1)
        _, messages = queue.get_first()
        self.assertEqual([obj_1], messages)
        queue.push(obj_2)
        _, messages = queue.get_first()
        self.assertEqual([obj_1], messages)
        _, messages = queue.get_first()
        self.assertNotEqual([obj_2], messages)
        queue.remove_first()
        _, messages = queue.get_first()
        self.assertEqual([obj_2], messages)
        queue.remove_first()
        _, messages = queue.get_first()
        self.assertListEqual(messages, [])

    def test_multiple(self):
        """
        Test that queue can work with multiple messages
        :return:
        """
        data = self._get_data(3)
        queue = PersistMessageQueue(self.cache_dir)
        queue.clear()

        for piece in data:
            queue.push(piece)

        leftover, cache_data = queue.get_first(2)
        self.assertEqual(leftover, 1)
        self.assertEqual(len(cache_data), 2)
        self.assertEqual(cache_data[0], data[0])
        self.assertEqual(cache_data[1], data[1])

        removed_data = queue.remove_first(3)
        self.assertEqual(len(removed_data), 3)
        _, messages = queue.get_first()
        self.assertListEqual(messages, [])

    def test_clear(self):
        """
        Test 'clear' possibility
        :return:
        """
        queue = PersistMessageQueue(self.cache_dir)
        queue.push("test")
        queue.clear()
        _, messages = queue.get_first()
        self.assertListEqual(messages, [])

    def test_is_empty(self):
        """
        Test empty check
        :return:
        """
        queue = PersistMessageQueue(self.cache_dir)
        queue.clear()
        self.assertTrue(queue.is_empty())
        queue.push("test")
        self.assertFalse(queue.is_empty())
