"""
Tests for ModBusTCP devices
"""

import unittest
from abc import ABC, abstractmethod
from typing import Any, List

from pyModbusTCP.server import ModbusServer
from pyModbusTCP.client import ModbusClient

from device.modbus_tcp.qm42vt2_tcp import QM42VT2TCPDevice
from device.modbus_tcp.m12fth3q_tcp import M12FTH3QTCPDevice


class BaseModBusTCPTest(unittest.TestCase, ABC):
    """
    Base class for ModBusTCP devices
    """

    server_address = 'localhost'
    server_port = 8080
    sensor_id = 1

    @property
    @abstractmethod
    def starting_address(self) -> int:
        """
        Stat holding register address
        :return: Stat holding register address
        """
        return 0

    @property
    @abstractmethod
    def raw_data(self) -> List[int]:
        """
        Raw data that will be stored in holding registers
        :return: Raw data that will be stored in holding registers
        """
        return []

    @property
    @abstractmethod
    def expected_data(self) -> List[Any]:
        """
        Data that should be read from device
        :return: Data that should be read from device
        """
        return []

    def setUp(self):
        self._modbus_server = ModbusServer(self.server_address, self.server_port, no_block=True)
        self._modbus_server.start()
        client = ModbusClient(self.server_address, self.server_port, self.sensor_id, auto_open=True,
                              auto_close=True)
        result = client.write_multiple_registers(self.starting_address, self.raw_data)
        self.assertTrue(result)

    def tearDown(self):
        self._modbus_server.stop()


class TestQM42VT2TCPDevice(BaseModBusTCPTest):
    """
    Tests for QM42VT2 over ModBusTCP
    """

    starting_address = 5200

    raw_data = [47, 119, 8307, 2837, 56, 142, 49, 38, 97, 97, 7, 7, 3076, 2936,
                4848, 3843, 66, 169, 79, 202, 10, 9]

    expected_data = [0.0047, 0.0119, 83.07, 28.37, 0.0056, 0.0142, 0.049, 0.038,
                     9.7, 9.7, 0.007, 0.007, 3.076, 2.936, 4.848, 3.843, 0.0066,
                     0.0169, 0.0079, 0.0202, 0.01, 0.009]

    def test_read(self):
        """
        Test read data
        :return:
        """
        device = QM42VT2TCPDevice('modbus_tcp', self.sensor_id, 1, self.server_address,
                                  self.server_port)
        data = device.read()
        self.assertListEqual(self.expected_data, data)


class TestM12FTH3QTCPDevice(BaseModBusTCPTest):
    """
    Tests for M12FTH3Q over ModBusTCP
    """

    starting_address = 0000

    raw_data = [5100, 1661, 567]
    expected_data = [51.0, 83.05, 28.35]

    def test_read(self):
        """
        Test read data
        :return:
        """
        device = M12FTH3QTCPDevice('modbus_tcp', self.sensor_id, 1, self.server_address,
                                   self.server_port)
        data = device.read()
        self.assertListEqual(self.expected_data, data)
