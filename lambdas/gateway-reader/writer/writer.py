"""
Writer class
"""

import time
import logging

from worker import BaseWorker, sleep


logger = logging.getLogger()


__all__ = ['Writer']


class Writer(BaseWorker):
    """
    Writer class
    Gets data from queue and tries to send it using connector
    """

    def __init__(self, message_queue, connector, encoder, send_interval, message_limit=1000,
                 error_interval=1, thread_name='WriterWorker'):
        super(Writer, self).__init__(thread_name=thread_name)
        self._message_queue = message_queue
        self._connector = connector
        self._encoder = encoder
        self._send_interval = send_interval
        self._message_limit = message_limit
        self._error_interval = error_interval

    @property
    def name(self):
        return self._encoder.name

    def _run(self, *args, **kwargs):
        num, messages = self._message_queue.get_first(self._message_limit)
        if not messages:
            logger.warning(f'Message queue is empty for "{self.name}", '
                           f'no messages to write')
            sleep(self._send_interval, self._shutdown_event)
            return
        send_start_time = time.time()
        num_send = self._connector.send(messages, self._encoder)
        send_time = time.time() - send_start_time
        if not num_send:
            logger.warning(f'Failed to send {len(messages)} messages to "{self.name}"')
            time.sleep(self._error_interval)
            return
        logger.info(f'Send {num_send}/{len(messages)} messages to "{self.name}" for '
                    f'{round(send_time, 1)} seconds')
        self._message_queue.remove_first(num_send)
        if num > 0:
            return
        sleep(self._send_interval, self._shutdown_event)
