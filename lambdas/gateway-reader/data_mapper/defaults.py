"""
Specifies default mapping for device -> table
"""

from device import ERROR_DEVICE_KEY, QM42VT2_DEVICE_KEY, M12FTH3Q_DEVICE_KEY, \
    QM42VT2_TCP_DEVICE_KEY, M12FTH3Q_TCP_DEVICE_KEY, CSV_PLC_DEVICE_KEY, \
    QM42VT2_EMULATED_DEVICE_KEY, M12FTH3Q_EMULATED_DEVICE_KEY, CSV_PLC_EMULATED_DEVICE_KEY, \
    QM42VT2_EMULATED_ML_ALARM_DEVICE_KEY, QM42VT2_EMULATED_MTD_ALARM_DEVICE_KEY, \
    QM42VT2_EMULATED_ISO_RED_ALARM_DEVICE_KEY, QM42VT2_EMULATED_ISO_YELLOW_ALARM_DEVICE_KEY
from table_schema import ERROR_SCHEMA_KEY, SENSOR_SCHEMA_KEY, AMBIENT_ENV_SCHEMA_KEY, PLC_SCHEMA_KEY
from utils import STRING_TYPE_KEY, FLOAT_TYPE_KEY, INT_TYPE_KEY


QM42VT2_SENSOR_MAPPING = [
    {'index': 0, 'type': FLOAT_TYPE_KEY},
    {'index': 2, 'type': FLOAT_TYPE_KEY},
    {'index': 4, 'type': FLOAT_TYPE_KEY},
    {'index': 6, 'type': FLOAT_TYPE_KEY},
    {'index': 7, 'type': FLOAT_TYPE_KEY},
    {'index': 8, 'type': FLOAT_TYPE_KEY},
    {'index': 9, 'type': FLOAT_TYPE_KEY},
    {'index': 10, 'type': FLOAT_TYPE_KEY},
    {'index': 11, 'type': FLOAT_TYPE_KEY},
    {'index': 12, 'type': FLOAT_TYPE_KEY},
    {'index': 13, 'type': FLOAT_TYPE_KEY},
    {'index': 14, 'type': FLOAT_TYPE_KEY},
    {'index': 15, 'type': FLOAT_TYPE_KEY},
    {'index': 16, 'type': FLOAT_TYPE_KEY},
    {'index': 18, 'type': FLOAT_TYPE_KEY},
    {'index': 20, 'type': FLOAT_TYPE_KEY},
    {'index': 21, 'type': FLOAT_TYPE_KEY},
]


M12FTH3Q_AMBIENT_MAPPING = [
    {'index': 0, 'type': FLOAT_TYPE_KEY},
    {'index': 2, 'type': FLOAT_TYPE_KEY},
]


CSVPLC_PLC_MAPPING = [
    {'index': 3, 'type': INT_TYPE_KEY},
    {'index': 4, 'type': FLOAT_TYPE_KEY},
    {'index': 5, 'type': FLOAT_TYPE_KEY},
    {'index': 6, 'type': FLOAT_TYPE_KEY},
    {'index': 7, 'type': INT_TYPE_KEY},
    {'index': 8, 'type': INT_TYPE_KEY},
    {'index': 9, 'type': INT_TYPE_KEY},
    {'index': 10, 'type': STRING_TYPE_KEY},
]

DEVICE_TABLE_MAPPINGS = {
    ERROR_DEVICE_KEY: {'table_key': ERROR_SCHEMA_KEY, 'mapping': None},

    QM42VT2_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    M12FTH3Q_DEVICE_KEY: {'table_key': AMBIENT_ENV_SCHEMA_KEY, 'mapping': M12FTH3Q_AMBIENT_MAPPING},
    CSV_PLC_DEVICE_KEY: {'table_key': PLC_SCHEMA_KEY, 'mapping': CSVPLC_PLC_MAPPING},

    QM42VT2_TCP_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    M12FTH3Q_TCP_DEVICE_KEY: {'table_key': AMBIENT_ENV_SCHEMA_KEY, 'mapping': M12FTH3Q_AMBIENT_MAPPING},

    QM42VT2_EMULATED_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    M12FTH3Q_EMULATED_DEVICE_KEY: {'table_key': AMBIENT_ENV_SCHEMA_KEY, 'mapping': M12FTH3Q_AMBIENT_MAPPING},
    CSV_PLC_EMULATED_DEVICE_KEY: {'table_key': PLC_SCHEMA_KEY, 'mapping': CSVPLC_PLC_MAPPING},

    QM42VT2_EMULATED_MTD_ALARM_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    QM42VT2_EMULATED_ML_ALARM_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    QM42VT2_EMULATED_ISO_YELLOW_ALARM_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING},
    QM42VT2_EMULATED_ISO_RED_ALARM_DEVICE_KEY: {'table_key': SENSOR_SCHEMA_KEY, 'mapping': QM42VT2_SENSOR_MAPPING}
}
