"""
Allows to transform raw data to some specified form
"""
from typing import List, Any

from .field import Field


__all__ = ['Mapping']


class Mapping():
    """
    Contain fields for data mapping
    """

    def __init__(self, fields):
        self._fields = fields

    @classmethod
    def from_json(cls, mapping: dict) -> 'Mapping':
        """
        Init new Mapping based on specification
        :param mapping: dict that describes mapping
        :return: Mapping instance
        """
        return cls([Field(**f) for f in mapping])

    @property
    def fields(self):
        """
        Fields in this mapping
        :return: fields
        """
        return self._fields

    def apply(self, data: List[Any]) -> List[Any]:
        """
        Transform raw 'data' according to mapping
        :param data: data to transform
        :return: processed data
        """
        return [f.py_type(data[f.index]) for f in self.fields]
