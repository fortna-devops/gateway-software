"""
Data Mapper error class
"""


class DataMapperError(Exception):
    """
    Data Mapper error class
    """
