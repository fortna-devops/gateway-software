"""
Holds all known mappers and allows to apply them by device key
"""

from typing import Tuple, Optional, Set, Iterable, Any, List

from schema import Schema, And, Or

from device import DEVICE_KEYS
from table_schema import AVAILABLE_SCHEMAS
from manager import BaseManager
from utils import contained_by, PY_TYPE_MAP
from .error import DataMapperError
from .defaults import DEVICE_TABLE_MAPPINGS
from .mapping import Mapping


__all__ = ['MappingManager']


class MappingManager(BaseManager):
    """
    Holds all known mappers and allows to apply them by device key
    """

    _validation_schema = Schema({
        And(str, contained_by(DEVICE_KEYS)): {
            'table_key': And(str, contained_by(AVAILABLE_SCHEMAS)),
            'mapping': Or(None, [{
                'index': And(int, lambda n: n >= 0),
                'type': And(str, contained_by(PY_TYPE_MAP))
            }])
        }
    })

    def __init__(self, mappings=None):
        if mappings is None:
            mappings = {}
        merged_mappings = self._validate(dict(DEVICE_TABLE_MAPPINGS, **mappings))

        self._mappings = {}
        for device_key, mapping_spec in merged_mappings.items():
            table_key = mapping_spec['table_key']
            if mapping_spec['mapping'] is not None and mapping_spec['mapping']:
                mapping = Mapping.from_json(mapping_spec['mapping'])
            else:
                mapping = None
            self._mappings[device_key] = (table_key, mapping)

    def _get_mapping(self, device_key: str) -> Tuple[str, Optional[Mapping]]:
        """
        Get mapping by device key
        :param device_key: device key to determine mapping
        :return: tuple of table key and 'Mapping'
        """
        if device_key not in self._mappings:
            raise DataMapperError(f'Unknown device key "{device_key}"')

        return self._mappings[device_key]

    def get_unique_table_keys(self, device_keys: Iterable[str]) -> Set[str]:
        """
        Get set of unique table keys based on the list of device keys
        :param device_keys: list of device keys
        :return: set of table keys
        """
        table_keys = set()
        for device_key in device_keys:
            table_key, _ = self._get_mapping(device_key)
            table_keys.add(table_key)
        return table_keys

    def apply(self, device_key: str, data: List[Any]) -> Tuple[str, List[Any]]:
        """
        Applies known mapper to data based on device_key
        :param device_key: Device name to select mapping
        :param data: Data to be modified
        :return: Table name, modified data
        """
        table_key, mapping = self._get_mapping(device_key)

        if mapping is not None:
            data = mapping.apply(data)

        return table_key, data
