"""
Specifies fields configuration
"""
from typing import Type

from utils import PY_TYPE_MAP
from .error import DataMapperError


__all__ = ['Field']


class Field():
    """
    Specifies fields configuration
    """

    def __init__(self, index, type):  # pylint: disable=W0622
        self._index = index
        self._type = type
        self._py_type = self._get_type(PY_TYPE_MAP, type)

    @staticmethod
    def _get_type(mapping: dict, f_type: str) -> type:
        _type = mapping.get(f_type)
        if not _type:
            raise DataMapperError(f'Unsupported field type "{f_type}"')
        return _type

    @property
    def index(self) -> int:
        """
        Index of field to get from data list
        :return: Index of field to get from data list
        """
        return self._index

    @property
    def type(self) -> str:
        """
        Type of field
        :return: Type of field
        """
        return self._type

    @property
    def py_type(self) -> Type:
        """
        Python type of field
        :return: Python type of field
        """
        return self._py_type
