"""
Message class
"""
from typing import Any, List

__all__ = ['Message']


class Message():
    """
    Message class

    Holds message data, type, and timestamp
    This Message is can be stored in local cache and send to the cloud
    """

    def __init__(self, m_type: str, data: List[Any], timestamp: float):
        self._m_type = m_type
        self._data = data
        self._timestamp = timestamp

    @property
    def m_type(self):
        """
        Message type
        :return:
        """
        return self._m_type

    @property
    def data(self):
        """
        Message data
        :return:
        """
        return self._data

    @property
    def timestamp(self):
        """
        Message timestamp
        :return:
        """
        return self._timestamp

    def __repr__(self):
        type_name = type(self).__name__
        arg_strings = []
        for name in ('m_type', 'timestamp', 'data'):
            arg_strings.append('%s=%r' % (name, getattr(self, name)))
        return '%s(%s)' % (type_name, ', '.join(arg_strings))
