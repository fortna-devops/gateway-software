"""
Base Worker class
"""

import threading
import logging
import time
from contextlib import contextmanager
from abc import ABC, abstractmethod
from typing import Optional

from .error import WorkerError


__all__ = ['BaseWorker']


logger = logging.getLogger()


class WorkerState():
    """
    Determine worker state
    """
    _new_state = 'new'
    _running_state = 'running'
    _stopped_state = 'stopped'
    _dead_state = 'dead'

    def __init__(self, worker: 'BaseWorker'):
        thread = worker._thread  # pylint: disable=W0212
        event = worker._shutdown_event  # pylint: disable=W0212
        if thread is None:
            self._state = self._new_state
        elif thread.is_alive():
            self._state = self._running_state
        elif event.is_set():
            self._state = self._stopped_state
        else:
            self._state = self._dead_state

    @property
    def state(self) -> str:
        """
        Worker state
        :return: State
        """
        return self._state

    @property
    def is_new(self) -> bool:
        """
        Check if worker is just initialized
        :return: True if worker is in "new" state
        """
        return self._state == self._new_state

    @property
    def is_running(self) -> bool:
        """
        Check if worker is up and running
        :return: True if worker is in "running" state
        """
        return self._state == self._running_state

    @property
    def is_stopped(self) -> bool:
        """
        Check if worker is stopped
        :return: True if worker is in "stopped" state
        """
        return self._state == self._stopped_state

    @property
    def is_dead(self) -> bool:
        """
        Check if worker is dead
        :return: True if worker is in "dead" state
        """
        return self._state == self._dead_state


class BaseWorker(ABC):
    """
    Base class for workers
    """

    def __init__(self, thread_name: str = 'Worker'):
        self._thread_name = thread_name
        self._thread = None
        self._shutdown_event = threading.Event()
        self._exception = None

    @property
    def exception(self) -> Optional[BaseException]:
        """
        Last error
        :return: last error
        """
        return self._exception

    @property
    def state(self) -> WorkerState:
        """
        Worker state
        :return: WorkerState instance
        """
        return WorkerState(self)

    @property
    def name(self) -> str:
        """
        Name of worker
        :return: Name of worker
        """
        return self._thread_name

    def start_worker(self, *args, **kwargs):
        """
        Start worker thread
        :param args: positional arguments for worker
        :param kwargs: named arguments for worker
        :return:
        """
        logger.info(f'Starting "{self._thread_name}"')
        if self.state.is_running:
            raise WorkerError('Reader thread already started')

        self._exception = None
        self._shutdown_event.clear()
        self._thread = threading.Thread(target=self._worker, args=args,
                                        kwargs=kwargs, name=self._thread_name)
        self._thread.setDaemon(True)
        self._thread.start()

    def stop_worker(self):
        """
        Stop worker thread
        :return:
        """
        logger.info(f'Stopping "{self._thread_name}"')
        if not self.state.is_running:
            raise WorkerError('Reader thread not started or already dead')
        self._shutdown_event.set()
        self._thread.join()

    def restart_worker(self):
        """
        Restart worker thread
        :return:
        """
        self.stop_worker()
        self.start_worker()

    @contextmanager
    def _log_error(self):
        try:
            yield
        except BaseException as e:  # pylint: disable=W0703
            logger.error(f'Error in "{self._thread_name}"', exc_info=e)
            self._exception = e

    def _worker(self, *args, **kwargs):
        while not self._shutdown_event.is_set():
            with self._log_error():
                self._run(*args, **kwargs)
                time.sleep(0.01)

    @abstractmethod
    def _run(self, *args, **kwargs):
        pass
