"""
Worker utility methods
"""

import threading
import time


__all__ = ['sleep']


def sleep(interval: float, shutdown_event: threading.Event):
    """
    Make thread sleep for specified interval or till event is set
    :param interval: time to sleep
    :param shutdown_event: event to check
    :return:
    """

    start_time = time.time()
    while time.time() - start_time < interval and not shutdown_event.is_set():
        time.sleep(.1)
