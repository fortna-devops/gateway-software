"""
Supervisor class
"""

import threading
import logging
import time

logger = logging.getLogger()


__all__ = ['Supervisor']


class Supervisor():
    """
    start, stop and monitor workers
    """

    def __init__(self, workers, worker_check_period=1):
        self._workers = workers
        self._worker_check_period = worker_check_period
        self._shutdown_event = threading.Event()

        self._last_check_time = time.time()

    def _check_workers(self):
        for worker in self._workers:
            if worker.state.is_dead:
                logger.error(f'Worker "{worker.name}" is dead. Restarting...')
                worker.start_worker()

        self._last_check_time = time.time()

    def _start_workers(self):
        for worker in self._workers:
            worker.start_worker()

    def _stop_workers(self):
        for worker in self._workers:
            worker.stop_worker()

    def start(self):
        """
        Start workers
        :return:
        """
        self._start_workers()

        while not self._shutdown_event.is_set():
            if time.time() - self._last_check_time >= self._worker_check_period:
                self._check_workers()

            time.sleep(0.01)

        self._stop_workers()

    def stop(self):
        """
        Stop workers
        :return:
        """
        self._shutdown_event.set()
