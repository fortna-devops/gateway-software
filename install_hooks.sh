#!/usr/bin/env bash

GIT_HOOKS_DIR=./.git/hooks/
HOOKS_DIR=../../hooks/*

cd ${GIT_HOOKS_DIR}

for hook in ${HOOKS_DIR}
do
    tmp=${hook##*/}
    ln -sf ${hook} ${tmp%.*}
    echo Sucessfully install ${tmp%.*} hook.
done
