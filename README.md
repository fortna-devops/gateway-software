# MHS gateway software

This repo is a storage for gateway software

## Before you start
### Structure
* Lambda should be placed in [lambdas](lambdas) dir
* Lambda should be represented by dir
* Lambda should have `.config.ini` file

### Initial configuration
This repo uses submodule `mhs-ci`
To update submodule run:
```
git submodule update --init
```

To update submodule automatically on `git pull` run:
```
git config submodule.recurse true
```
You can add `--global` to make this system-wide (for all repos).

Also do not forget to install git hooks:
```
./mhs-ci/install_hooks.sh
```


## How to use
After you push code to this repo `pipeline` will be run according to the [bitbucket-pipelines](bitbucket-pipelines.yml)

## Additional information
To get more details about CI please read the readme for `mhs-ci` submodule.

Each lambda contains it's own readme as well.
